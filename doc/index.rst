.. VImPy documentation master file, created by
   sphinx-quickstart2 on Wed Mar 27 12:45:57 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VImPy's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 4

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

