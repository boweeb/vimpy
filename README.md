# This is my README

=====
VImPy
=====

Copyright (c) 2013, Jesse Butcher
See LICENSE

-------------
Windows build
-------------

Summary: pyinstaller is used to compile a self-contained, distributable directory containing binaries and libraries.

1) Pull most recent commit
2) Run "build_vimpy.bat" batch script
	a) Removes build files
	b) Runs "pyinstaller.py" with the "vimpy.spec" file
3) Distribute compiled directory:
	C:\Users\<user>\<path-to>\pyinstaller-2.0\vimpy\dist


----------------------
Contents of vimpy.spec
----------------------

# -*- mode: python -*-
a = Analysis(['C:\\Users\\administrator\\Desktop\\vimpy\\main.py'],
             pathex=['C:\\Users\\administrator\\Desktop\\vimpy',
			         'C:\\Users\\administrator\\Downloads\\pyinstaller-2.0',
					 'C:\\Python27\\Lib\\site-packages'],
             hiddenimports=['pyodbc'],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\vimpy', 'vimpy.exe'),
          debug=False,
          strip=None,
          upx=False,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=False,
               name=os.path.join('dist', 'vimpy'))


---------------------------
Contents of build_vimpy.bat
---------------------------
@echo off

CD "C:\Users\administrator\Downloads\pyinstaller-2.0"
RMDIR /S /Q vimpy\build
RMDIR /S /Q vimpy\dist

pyinstaller.py vimpy/vimpy.spec
PAUSE
