# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Feb 15, 2013

@author: Jesse Butcher

toolbox.screwdriver:
    This tool contains functions useful for ui forms.
'''

# import cryptacular
import cStringIO
import sys

from PyQt4 import QtCore
from PyQt4 import QtGui

from vimpy import G

#from vimpy.toolbox.wrench import report_to_console
#
## Aliases
#rpt = report_to_console


###############################################################################
# FUNCTIONS -- Home
###############################################################################
def get_selected(control):
    """Create list of selected items in a QTreeWidget."""
    # Get list of selected files
    download_list = []
    root = control.invisibleRootItem()
    child_count = root.childCount()
    for i in range(child_count):
        item = root.child(i)
        if item.checkState(0):
            item_text = str(item.text(0))
            download_list.append(item_text)

    return download_list


def select_all(control):
    root = control.invisibleRootItem()
    child_count = root.childCount()
    for i in range(child_count):
        item = root.child(i)
        item.setCheckState(0, Qt.Checked)
    return


###############################################################################
# FUNCTIONS -- Options
###############################################################################
def save_opt_scaf(section, options):
    """
    Save Options Scaffold
        options = array of tuples:
            [(option, control), ...]
        TODO: Currently only supports text controls (QLineEdit)
    """
    if not G.config.has_section(section):
        G.config.add_section(section)
    for option in options:
        value = str(option[1].text())
        G.config.set(section, option[0], value)


def save_options(parentform):
    """Save Options"""
    p = parentform
    ### SOURCE ###
    save_opt_scaf("db_src", [("host", p.txtSrcHost),
                             ("user", p.txtSrcUser),
#                              ("pass", p.txtSrcPass),
                             ("port", p.txtSrcPort),
                             ("schema", p.txtSrcSchema)])
    ### DESTINATION ###
    save_opt_scaf("db_dst", [("host", p.txtDstHost),
                             ("user", p.txtDstUser),
#                              ("pass", p.txtDstPass),
                             ("port", p.txtDstPort),
                             ("dsn", p.txtDstDsn)])

    #Write config file
    with open(G.config_file, 'w+b') as configfile:
        G.config.write(configfile)


def load_opt_scaf(section, options):
    """"
    Load Options Scaffold
        options = array of tuples:
            [(option, control), ...]
        TODO: Currently only supports text controls (QLineEdit)
    """
    if G.config.has_section(section):
        for option in options:
            if G.config.has_option(section, option[0]):
                option[1].setText(G.config.get(section, option[0]))


def load_options(parentform):
    """Load Options"""
    p = parentform
    ### SOURCE ###
    load_opt_scaf("db_src", [("host", p.txtSrcHost),
                             ("user", p.txtSrcUser),
#                              ("pass", p.txtSrcPass),
                             ("port", p.txtSrcPort),
                             ("schema", p.txtSrcSchema)])
    ### DESTINATION ###
    load_opt_scaf("db_dst", [("host", p.txtDstHost),
                             ("user", p.txtDstUser),
#                              ("pass", p.txtDstPass),
                             ("port", p.txtDstPort),
                             ("dsn", p.txtDstDsn)])


def update_pw(control, section):
    value = control.text().toUtf8().data()
    
    # Encode password string.
    buff_ = cStringIO.StringIO(G.crypter.Encrypt(value))
    
    G.config.set(section, "pass", buff_.read())
    
    buff_.close()

    #Write config file
    with open(G.config_file, 'w+b') as configfile:
        G.config.write(configfile)


###############################################################################
# FUNCTIONS -- Error Dialogs
###############################################################################
def cfg_warnbox(title, message):
    print "--- MessageBox ---"
    question = ('\n\n' +
                "Would you like to open the Options dialog now for review?")
    message = message + question
    box = QtGui.QMessageBox.warning(None,
                               'VImPy | ' + title,
                               message,
                               buttons = (QtGui.QMessageBox.Open |
                                          QtGui.QMessageBox.No)
                               )
    if   (box == QtGui.QMessageBox.No):
        response = 'No'
    elif (box == QtGui.QMessageBox.Open):
        response = 'Open'

        # Open the Options dialog.
        from vimpy.ui.options import diaOptions
        dlg = diaOptions()
        dlg.exec_()

    print "\tResponse: " + response
    print "---"

def gen_errorbox(title, message):
    print "--- MessageBox ---"
    x = QtGui.QMessageBox.critical(None,
                                   'VImPy | ' + title,
                                   message,
                                   buttons = (QtGui.QMessageBox.Ok)
                                   )
    print "    Response: " + str(x)
    print "---"

def det_errorbox(title, message, details):
    print "--- MessageBox ---"
    msgbox = QtGui.QMessageBox()
    msgbox.setWindowTitle('VImPy | ' + title)
    msgbox.setText(message)
    msgbox.setDetailedText(details)
    msgbox.setStandardButtons(QtGui.QMessageBox.Ok)
    msgbox.setIcon(QtGui.QMessageBox.Critical)
    msgbox.exec_()

def gen_warnbox(title, message):
    print "--- MessageBox ---"
    x = QtGui.QMessageBox.warning(None,
                                 'VImPy | ' + title,
                                 message,
                                 buttons = (QtGui.QMessageBox.Ok)
                                 )
    print "    Response: " + str(x)
    print "---"

# def show_err(title, message):
#     diag = QtGui.QErrorMessage
#     s = None
#     while s is None:
#         qstring, ok = diag.getText(control,
#                                    QtCore.QString(title),
#                                    QtCore.QString(text),
#                                    mode=QtGui.QLineEdit.Password)
#         s = str(qstring)
#         if ok is False: # user pressed Cancel
#             return None
#         if s == '':     # user entered nothing
#             s = None
#     return s
