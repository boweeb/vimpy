# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Mar 26, 2013

@author: Jesse Butcher

toolbox.pocketwatch:
    This tool contains methods for timestamp conversion.
      Returns a generator for list iteration.

"""

import datetime
from datetime import datetime as d


###############################################################################
# GENERATOR
###############################################################################
def time2gen(data, format_in, format_out):
    """ Convert time formats. Supports mssql, epoch, simple, and dt. """
    str_formats = {'mssql': '%m/%d/%Y %I:%M:%S %p',
                   'simple': '%m/%d/%Y',
                   'file_suffix': '%Y%m%d'
                   }
    # Define valid string output formats. 
    valid_str_out = str_formats.keys() + ['epoch']
    
    if (format_in in str_formats.keys()) and (format_out in valid_str_out):
        for i in data:
            if format_out == 'epoch':
                yield d.strftime(d.strptime(i, str_formats[format_in]), '%s')
            else:
                yield d.strftime(d.strptime(i, str_formats[format_in]),
                                 str_formats[format_out])
    elif (format_out == 'datetime') and (format_in != 'epoch'):
        for i in data:
            yield d.strptime(i, str_formats[format_in])
    elif format_in == 'epoch':
        for i in data:
            dt = d.fromtimestamp(int(i))
            if format_out == 'datetime':
                yield dt
            else:
                yield dt.strftime(str_formats[format_out])
    elif format_in == 'datetime':
        for i in data:
            if format_out == 'epoch':
                yield d.strftime(i, '%s')
            else:
                yield d.strftime(i, str_formats[format_out])
    else:
        raise NotImplementedError


def round_time(dt=None, round_to=60):
    """Round a datetime object to any time laps in seconds
    dt : datetime.datetime object, default now.
    round_to : Closest number of seconds to round to, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
    http://stackoverflow.com/questions/3463930/...
           how-to-round-the-minute-of-a-datetime-object-python/10854034#10854034
    Returns: <datetime.datetime> obj.
    """
    if dt is None:
        dt = datetime.datetime.now()
    seconds = (dt - dt.min).seconds
    rounding = (seconds + round_to / 2) // round_to * round_to
    return dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)


