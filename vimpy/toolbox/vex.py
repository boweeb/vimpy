# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Mar 29, 2013

@author: Jesse Butcher

toolbox.vex:
    [v]impy [ex]ceptions -- This tool contains custom exceptions.

"""


class MyError(Exception):
    """Base class for exceptions in this module."""
    pass


class MalformedDataError(MyError):
    """Raised when an iterable is ill-defined.

    Attributes:
        idx   -- if exists, index number of bad field, else '-1'
        field -- the offending field
        msg   -- plain text explanation

    """

    def __init__(self, idx='', field='', msg=''):
        self.idx = idx
        self.field = field
        self.msg = msg

        params = [idx, field, msg]
        description = 'Index: {0}, Field: {1}\n{2}'

        MyError.__init__(self, description.format(*params))
