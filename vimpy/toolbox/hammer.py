# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Feb 15, 2013

@author: Jesse Butcher

toolbox.hammer:
    This tool contains functions useful for using ssh with paramiko or spur.

"""

import os
import types
import datetime
import time
import json
import operator
import re
from sqlalchemy import desc, exc


from PyQt4.QtGui import QTreeWidgetItem
from PyQt4.QtGui import QProgressDialog
from PyQt4.QtCore import Qt, QAbstractTableModel, QVariant
from PyQt4.QtCore import QCoreApplication
from PyQt4.QtCore import SIGNAL

from vimpy import G
from vimpy.toolbox.vex import MalformedDataError
from vimpy.toolbox.wrench import (report_to_console as rpt, hash_file,
                                  nullz, sanitize_uni)
from vimpy.toolbox import screwdriver

from vimpy.toolbox.pocketwatch import *

# Delayed catalog import. See init_catalog().
# from vimpy.toolbox import catalog as c


###############################################################################
# CLASSES
###############################################################################
class MyTableModel(QAbstractTableModel):
    """ The main presentation table. """
    def __init__(self, parent, hdr_data, tbl_data, *args, **kwargs):
        # Lesson: Calling "super()" == calling the model's base in a nicer way.
        #         Keeping the old line as reference (it's still equal).
        QAbstractTableModel.__init__(self, parent, *args, **kwargs)
#         super().__init__(parent)
        self.hdr_data = hdr_data
        self.tbl_data = tbl_data

    def rowCount(self, parent):
        return len(self.tbl_data)

    def columnCount(self, parent):
        return len(self.tbl_data[0])

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.hdr_data[col])
        return QVariant()

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:
            return QVariant()
        return QVariant(self.tbl_data[index.row()][index.column()])

    def sort(self, Ncol, order):
        self.emit(SIGNAL("layoutAboutToBeChanged()"))
        self.tbl_data = sorted(self.tbl_data, key=operator.itemgetter(Ncol))
        if order == Qt.DescendingOrder:
            self.tbl_data.reverse()
        self.emit(SIGNAL("layoutChanged()"))


###############################################################################
# FUNCTIONS
###############################################################################
def init_catalog():
    """
    Prevent catalog from being initialized on application load. This allows the
    application's main form to get fully initialized before dialogs are called.
    """
    from vimpy.toolbox import catalog as c
    return c


def render_table(parent_form, table, data, header):
    """ """
    # Trap zero-length data.
    # dummy_data let's the model evaluate the length of the first index
    #   by making a list that simply contains a nested empty list.
    dummy_data = [[]]
    if len(data) == 0:
        send_data = dummy_data
    else:
        send_data = data

    # Set the table model
    tbl_model = MyTableModel(parent_form, header, send_data)
    table.setModel(tbl_model)

    # Hide the vertical header
    vh = table.verticalHeader()
    vh.setVisible(False)

    # set horizontal header properties
    hh = table.horizontalHeader()
    hh.setStretchLastSection(True)

    # Set column width to fit contents
    table.resizeColumnsToContents()

    # Set row height
    nrows = len(send_data)
    for row in xrange(nrows):
        table.setRowHeight(row, 18)

    return 0


def init_dst_table(parent_form, table, header, tbl_name):
    """ Grab summary data from the source database. """
    if not G.engines_ready:
        rpt(parent_form, "ERROR", "Engines not ready.")
        return False
    else:
        c = init_catalog()

    session = c.Session()
    rpt(parent_form, "Initializing Table", tbl_name)

    qry_resources = session.query(c.Resource)
#     qry_r_ordered = qry_resources.order_by(desc(c.Resource.udWebPKID))
    qry_r_ordered = qry_resources.order_by(desc(c.Resource.HRRef))
    lst_resultset = qry_r_ordered.filter(c.Resource.udWebPKID != None).all()
    rpt(parent_form, '\t' + tbl_name, "Created resource query objects.")

    lst_keys = []
    for x in lst_resultset:
        lst_keys.append(x.udWebPKID)

    itr_epoch = iter(lst_keys)
    gen_simple = time2gen(lst_keys, "epoch", "simple")
    rpt(parent_form, '\t' + tbl_name, "Initialized generators.")

    data = []
    for x in lst_resultset:
        row = []
        row.append(str(itr_epoch.next()))
        row.append(gen_simple.next())
        row.append(x.HRRef)
        row.append(x.FirstName)
        row.append(x.LastName)
        row.append(x.udAppPosition)
        data.append(row)
    rpt(parent_form, '\t' + tbl_name, "Populated data array.")

    render_table(parent_form, table, data, header)
    rpt(parent_form, '\t' + tbl_name, "Rendered table.")

    # Send 'data' up to the form scope.
    return data, lst_keys


def init_src_table(parent_form, table, header, tbl_name, destination_data):
    """ Grab summary data from the source database. """
    if not G.engines_ready:
        rpt(parent_form, "ERROR", "Engines not ready.")
        return False
    else:
        c = init_catalog()

    progress_bar = QProgressDialog("Fetching new record details...",
                                   "Cancel", 0, 0, parent_form)
    progress_bar.setWindowTitle("Please wait")
    progress_bar.setWindowModality(Qt.WindowModal)
    progress_bar.resize(300, 100)
#     progress_bar.exec_()
    progress_bar.show()
    QCoreApplication.processEvents()

    session = c.Session()
    rpt(parent_form, "Initializing Table", tbl_name)

    # Get list of dst pkid's
    destination_keys = destination_data[1]

    # Load ignore data if file exists, else create empty file and data.
    try:
        with open(G.ignore_file, 'r') as f:
            ignores = json.load(f)
    except IOError:
        with open(G.ignore_file, 'w+') as f:
            f.write('{}')
            ignores = {}

    rpt(parent_form, "\tDEBUG", "Ignore list: " + str(ignores.keys()))

    total_ignore = destination_keys + ignores.keys()

    # Find un-imported, distinct primary keys:
    qry_ = session.query(c.Element.submit_time)
    qry_Frm = qry_.filter(c.Element.form_name == 'Application')
    qry_Not = qry_Frm.filter(c.Element.submit_time.notin_(total_ignore))
    qry_Dnt = qry_Not.distinct()
#     r_count = qry_Dnt.count()
    r_distinct = qry_Dnt.all()

    if len(r_distinct) == 0:
        data = []
        render_table(parent_form, table, data, header)
        progress_bar.close()
        return data

    # r_distinct is a list of keyed tuples.
    # Convert to plain list of decimal numbers.
    lst_ids = []
    for x in r_distinct:
        lst_ids.append(x[0])

    gen_simple = time2gen(lst_ids, "epoch", "simple")
    rpt(parent_form, '\t' + tbl_name, "Initialized generators.")

    auxiliary_fields = ['name-last', 'name-first', 'position']

    qry = session.query(c.Element.submit_time,
                        c.Element.field_name,
                        c.Element.field_value,
                        c.Element.form_name)
    qry_form = qry.filter(c.Element.form_name == 'Application')
    qry_order = qry_form.order_by(c.Element.field_order)
    qry_field = qry_order.filter(c.Element.field_name.in_(auxiliary_fields))

    rpt(parent_form, '\t' + tbl_name, "Created resource query objects.")

    q = qry_field.filter(c.Element.submit_time.in_(lst_ids))

    # Using dictionary to take advantage of keys.
    data_dict = {}
    for r in q.all():
        id = str(r.submit_time)
        # If record with this PKID doesn't exist yet:
        if not id in data_dict:
            # Init blank record
            data_dict[id] = [gen_simple.next(), None, None, None]
        # Select case by field
        if r.field_name == 'name-first':
            data_dict[id][1] = r.field_value
        elif r.field_name == 'name-last':
            data_dict[id][2] = r.field_value
        elif r.field_name == 'position':
            data_dict[id][3] = r.field_value

    # Flatten dictionary into list-o-lists for table presentation.
    data = []
    sorted_keys = sorted(data_dict.iterkeys())
    for k in sorted_keys:
        row = []
        row.append(k)
        for i in data_dict[k]:
            row.append(i)
        data.append(row)

    rpt(parent_form, '\t' + tbl_name, "Populated data array.")

    render_table(parent_form, table, data, header)
    rpt(parent_form, '\t' + tbl_name, "Rendered table.")

    # Send 'data' up to the form scope.
    progress_bar.close()
    return data


def assign_value(parent_form, rsrc, tbl_key, fld_key, fld_value):
    """ Assign attribute to model instance. """
    # "field" here is the *destination* field. "fld_key" is the *source* field.
    field = G.vp_map[tbl_key][fld_key]
    value = fld_value

    byn_fields = ['relatives', 'emp1-contact', 'previous', 'functions',
                  'citizen', 'emp2-contact', 'school-high-graduate',
                  'school-college-graduate', 'school-grad-graduate', 'guilty',
                  'military']

    # Turn empty strings into nulls.
    if fld_value == '':
        value = None

    # AND

    # Field-specific transformation/conversion instructions:
    if fld_key in byn_fields:

        if value == 'yes':
            new_value = 'Y'
        elif value == 'no':
            new_value = 'N'
        else:
#             msg1 = "bYN field '{}' encountered with ".format(fld_key)
#             msg2 = "no value and no default."
#             rpt(parent_form, '\tWARNING', msg1 + msg2)
            new_value = None

        # Fields with a reverse definition True<=>False, Y<=>N
        reverse_byn = ['emp1-contact']
        if fld_key in reverse_byn:
            if new_value == 'Y':
                rvr_value = 'N'
            elif new_value == 'N':
                rvr_value = 'Y'

        setattr(rsrc, field, new_value)

    elif fld_key == 'sex':
        new_value = value[:1].upper()
        setattr(rsrc, field, new_value)

    elif fld_key == 'race':
        if value == 'Non-Minority / White':
            new_value = 'W '
        elif value == 'African American / Black':
            new_value = 'B '
        elif value == 'Hispanic / Latino':
            new_value = 'H '
        elif value == 'Native American or Alaskan Native':
            new_value = 'I '
        elif value == 'Asian / Pacific Islanders':
            new_value = 'A '
        else:
            raise ValueError("Race value unhandled: 'race' = {}".format(value))
        setattr(rsrc, field, new_value)

    else:
        # Truncate long strings to their max size.
        if type(fld_value) == types.StringType:
            value = sanitize_uni(parent_form, value)

    #         fld_max_len = getattr(rsrc, field).property.columns[0].type.length
            fld_max_len = rsrc.metadata.tables[tbl_key].columns[field].type.length
            if len(fld_value) > fld_max_len:
                value = fld_value[:fld_max_len]

        setattr(rsrc, field, value)

    return 0


def calc_hrref(parent_form, session):
    """ Calculate appropriate HRRef. """
    if not G.engines_ready:
        rpt(parent_form, "ERROR", "Engines not ready.")
        return False
    else:
        c = init_catalog()

    #Find last HRRef number of this year.
    lower_bound = int(datetime.date.today().strftime('%y')) * 1000
    upper_bound = lower_bound + 999
    qry = session.query(c.Resource.HRRef)
    qry_lower = qry.filter(c.Resource.HRRef >= lower_bound)
    qry_upper = qry_lower.filter(c.Resource.HRRef <= upper_bound)
    qry_order = qry_upper.order_by(desc(c.Resource.HRRef)).first()

    if qry_order is None:
        # Convention has new year indexes starting at xx001, not xx000.
        next_hrref = lower_bound + 1
    else:
        next_hrref = qry_order[0] + 1

    return next_hrref


def add_src_data(parent_form, rows):
    """ Fetch src data and add to the form table. """
    if not G.engines_ready:
        rpt(parent_form, "ERROR", "Engines not ready.")
        return False
    else:
        c = init_catalog()

    progress_bar = QProgressDialog("Importing records...",
                                   "Cancel", 0, 0, parent_form)
    progress_bar.setWindowTitle("Please wait")
    progress_bar.setWindowModality(Qt.WindowModal)
    progress_bar.resize(300, 100)
#     progress_bar.exec_()
    progress_bar.show()
    QCoreApplication.processEvents()

    session = c.Session()
    num_rows = len(rows)

    progress_bar.setMaximum(num_rows)

    # Save flattened list of mapped fields to import.
    mapped_fields = []
    for x in G.vp_map.keys():
        for y in G.vp_map[x].keys():
            if not (y == 'TAGS'):
                mapped_fields.append(y)

    rpt(parent_form, "Importing {} Record(s)".format(num_rows), "...")

    # Save selection as simple list of primary keys.
    lst_import = []
    for i in rows:
        lst_import.append(i.data().toString())

    # Create base query object on Element (src) model.
    qry = session.query(c.Element)
    # Filter to 'Application' form.
    qry_form = qry.filter(c.Element.form_name == 'Application')
    # Double-check fields will be presented in order.
    qry_order = qry_form.order_by(c.Element.field_order)
    # Restrict to fields defined in the map.
    qry_field = qry_order.filter(c.Element.field_name.in_(mapped_fields))

    # For each selected record:
    for i, selection in enumerate(lst_import):
        # Instantiate a record objects.
        rsrc = c.Resource()

        # Instantiate state indicators
        refs = {'r1': {'ref': c.Reference(), 'status': False},
                'r2': {'ref': c.Reference(), 'status': False},
                'r3': {'ref': c.Reference(), 'status': False}}

        # Filter query to current 'selection'.
        #   NOTE: Although 'selection' is a QString object,
        #         the comparison still works.
        q = qry_field.filter(c.Element.submit_time == selection)

        # Iterate 'selection's 'record's.
        for record in q.all():
            # Iterate through vp_map:
            # For each map table.
            for tbl_key in G.vp_map.keys():
                # For each field mapping.
                for fld_key in G.vp_map[tbl_key].keys():
                    # If the field name matches the field mapping.
                    if record.field_name == fld_key:
                        if tbl_key == 'HRRM':
                            # Assign field value to Resource object.
                            # Catch field conversions here:
                            assign_value(parent_form,
                                         rsrc,
                                         tbl_key, fld_key,
                                         record.field_value)

                        elif tbl_key == 'HRAR':
                            if ((record.field_value == "")
                                or (record.field_value is None)):
                                pass
                            else:
                                if record.field_name[3:4] == '1':
                                    if not refs['r1']['status']:
                                        refs['r1']['ref'] = c.Reference()
                                        refs['r1']['ref'].Seq = 1
                                        refs['r1']['ref'].HRCo = 1
                                        refs['r1']['status'] = True
                                    assign_value(parent_form,
                                                 refs['r1']['ref'],
                                                 tbl_key, fld_key,
                                                 record.field_value)

                                elif record.field_name[3:4] == '2':
                                    if not refs['r2']['status']:
                                        refs['r2']['ref'] = c.Reference()
                                        refs['r2']['ref'].Seq = 2
                                        refs['r2']['ref'].HRCo = 1
                                        refs['r2']['status'] = True
                                    assign_value(parent_form,
                                                 refs['r2']['ref'],
                                                 tbl_key, fld_key,
                                                 record.field_value)

                                elif record.field_name[3:4] == '3':
                                    if not refs['r3']['status']:
                                        refs['r3']['ref'] = c.Reference()
                                        refs['r3']['ref'].Seq = 3
                                        refs['r3']['ref'].HRCo = 1
                                        refs['r3']['status'] = True
                                    assign_value(parent_form,
                                                 refs['r3']['ref'],
                                                 tbl_key, fld_key,
                                                 record.field_value)

                                else:
                                    print "tbl_key known but ref# unknown."

                        else:
                            print "Unknown tbl_key"

        # Add default attributes/values.
        date_mssql = (time2gen([selection.toDouble()[0]],
                               "epoch",
                               "mssql")).next()
        date_datetime = (time2gen([selection.toDouble()[0]],
                                  "epoch",
                                  "datetime")).next()

        if selection.toDouble()[1]:
            date_epoch = selection.toDouble()[0]
        else:
            raise ValueError("Epoch conversion failed.")

        # XXX: Not sure why I used 'setattr' here instead of regular '='
#         setattr(rsrc, 'ApplicationDate', date_datetime)
#         setattr(rsrc, 'udWebPKID', date_epoch)

        rsrc.ApplicationDate = date_datetime
        rsrc.udWebPKID = date_epoch

        rsrc.HRRef = calc_hrref(parent_form, session)
        rsrc.SortName = (rsrc.LastName +
                         rsrc.FirstName
                         ).upper()[:15]

        rpt(parent_form, "\tImporting Applicant", str(rsrc))

        # Attach references to resource.
        for r in refs.itervalues():
            if r['status'] is True:
                rsrc.references.append(r['ref'])
                msg = "Appending Reference: {}".format(repr(r['ref']))
                rpt(parent_form, "\t\tHRAR", msg)

        # Add current resource record to the Session.
        session.add(rsrc)

        # Download resume for manual import.
        q_rsm = qry_order.filter(c.Element.field_name == 'resume')
        q = q_rsm.filter(c.Element.submit_time == selection)
        if q.count() == 1:
            for record in q.all():
                if (record.field_value is None) or (record.field_value == ""):
                    rpt(parent_form, "\tResume", "No resume.")
                else:
                    rpt(parent_form, "\tResume", "Exists. Saving locally.")
                    directory = G.resume_dir
                    f_ext = G.re_ext.sub(r"\1", record.field_value)
                    f_name = "_".join([str(rsrc.HRRef),
                                      rsrc.FirstName,
                                      rsrc.LastName])
                    fp = os.path.join(directory, f_name + f_ext)

                    with open(fp, "w+b") as f:
                        f.write(record.file)

                    rpt(parent_form, "\t\tFile", f_name + f_ext)
        elif q.count() != 1:
            screwdriver.gen_errorbox("Error",
                                     "Expected resume filter count = 0 or 1")

        progress_bar.setValue(i)
        QCoreApplication.processEvents()
        if progress_bar.wasCanceled():
            break

    # Commit all created Resource objects.
    session.commit()

    progress_bar.close()

    return 0
