# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Feb 15, 2013

@author: Jesse Butcher

toolbox.jig:
    This tool pulls in config options and creates globals.

"""

import os
import re
import ConfigParser
from keyczar import keyczar
from keyczar import keyczart


def dir_check(directory):
    """ Check if directory exists, if not then create it. """
    # http://stackoverflow.com/questions/273192/...
    #   python-best-way-to-create-directory-if-it-doesnt-exist-for-file-write
    
    print "Checking \"{}\" exists... ".format(directory),
    try: 
        os.makedirs(directory)
        print "False. Created directory."
    except OSError:
        if not os.path.isdir(directory):
            raise
        print "True. Directory exists."


def safe_init(caller, parser, global_var, section, option):
    """ Safely init variables, trapping parser errors. """
    # Goal (example):
    # self.src_host = self.config.get("db_src", "host")
    
    # The loop structure causes a *retry* after exception.
    while True:
        try:
            if not parser.has_section(section):
                parser.add_section(section)
            setattr(caller, global_var, parser.get(section, option))
        except ConfigParser.NoOptionError, e:
            print "Encountered an un-configured option. {}".format(e)
            parser.set(section, option, "Fill me.")
            continue
        break


def safe_init_dict_wrapper(caller, parser, option_array):
    """ High level option loader that parses a dictionary of options """
    o = option_array
    print "Parsing {} option section(s): {}... ".format(len(o),
                                                        o.keys()),
    for s in o.keys():
        if o[s]['type'] == 'database':
            abr = o[s]['abr']
            section = 'db_' + abr
            for field in o[s]['fields']:
                global_var = abr + '_' + field
                safe_init(caller, parser, global_var, section, field)
    print "Done."
        

class GlobalSwatch():
    """ The all-knowing, omniscient 'G'. """

    def __init__(self):
        """ Constructor """

        #######################################################################
        # LOAD CONFIG
        #######################################################################
        # Writing our configuration file to '~/.vimpy/settings.cfg'
        self.app_dir = os.path.join(os.path.expanduser("~"), ".vimpy")
        dir_check(self.app_dir)

        self.config_file = os.path.join(self.app_dir, "settings.cfg")

        self.config = ConfigParser.SafeConfigParser()
        # And if settings.cfg doesn't exist, init a blank one.
        if not os.path.exists(self.config_file):
            print "Config file missing. Initializing new, blank one."
            with open(self.config_file, "w") as fp:
                fp.write("[db_src]\n" +
                         "host = \n" +
                         "user = \n" +
                         "pass = \n" +
                         "port = \n" +
                         "schema = \n" +
                         "\n" +
                         "[db_dst]\n" +
                         "host = \n" +
                         "user = \n" +
                         "pass = \n" +
                         "port = \n" +
                         "dsn = \n") 

        self.config.read(self.config_file)

        #######################################################################
        # GLOBALS
        #######################################################################
        # Available in the options UI:
        option_array = {
            'Source DB': {
                'type': 'database',
                'abr': 'src',
                'fields': (
                    'host',
                    'user',
                    'pass',
                    'port',
                    'schema'
                )
            },
            'Destination DB': {
                'type': 'database',
                'abr': 'dst',
                'fields': (
                    'host',
                    'user',
                    'pass',
                    'port',
                    'dsn'
                )
            }
        }

        safe_init_dict_wrapper(self, self.config, option_array)

        # Not included in the options UI:
        self.ignore_file = os.path.join(self.app_dir, "ignore.json")
        self.key_loc = os.path.join(self.app_dir, "kz")
        dir_check(self.key_loc)
        self.resume_dir = os.path.join(self.app_dir, "resumes")
        dir_check(self.resume_dir)
        
        # Init status for ready engines.
        self.engines_ready = False

        # If the key meta doesn't exist: create it.
        path_meta = os.path.join(self.key_loc, 'meta')
        if not os.path.exists(path_meta):
            purpose = keyczar.keyinfo.DECRYPT_AND_ENCRYPT
            keyczart.Create(self.key_loc, 'vimpy', purpose)

        # If the primary key doesn't exist: create it.
        path_key = os.path.join(self.key_loc, '1')
        if not os.path.exists(path_key):
            keyczart.AddKey(self.key_loc, 'PRIMARY')

        # Derived:
        self.crypter = keyczar.Crypter.Read(self.key_loc)
#         self.dst_buff = cStringIO.StringIO(
#                         self.crypter.Decrypt(self.dst_pass))
#         self.src_buff = cStringIO.StringIO(
#                         self.crypter.Decrypt(self.src_pass))

        # Regex objects:
        # File extension, including dot.
        self.re_ext = re.compile(r".*(\.[^.\r\n]+)$", re.MULTILINE)
        # Control characters
        control_chars = ''.join(map(unichr, range(0, 32) + range(127, 160)))
        self.control_char_re = re.compile('[%s]' % re.escape(control_chars))

        #######################################################################
        # FIELD MAP
        #######################################################################
        # The fields are (mostly) presented in the order they're
        #   exported/received. Some are commented out b/c they exist
        #   but aren't used.

        self.vp_map = {'HRRM': {
            'name-first': 'FirstName',
            'name-last': 'LastName',
            'address1': 'Address',
            'city': 'City',
            'state': 'State',
            'zip': 'Zip',
            'phone-home': 'Phone',
            'phone-cell': 'CellPhone',
            'email': 'Email',
            'position': 'udAppPosition',
            'date-available': 'udAvailableDate',
            'salary': 'udExpectedSalary',
            'relatives': 'RelativesYN',
            'relatives-more': 'udRelativesMore',
            #                                 'previous-more': 'udPreviousMore',
            'hear': 'Source',
            'emp1-name': 'CurrentEmployer',
            'emp1-address': 'udEmp1Address',
            'emp1-city': 'udEmp1City',
            'emp1-state': 'udEmp1State',
            'emp1-zip': 'udEmp1Zip',
            'emp1-phone': 'udEmp1Phone',
            'emp1-position': 'udEmp1Position',
            'emp1-date-from': 'udEmp1DateFrom',
            'emp1-date-to': 'udEmp1DateTo',
            'emp1-supervisor': 'udEmp1Supervisor',
            'emp1-pay-start': 'udEmp1PayStart',
            'emp1-pay-end': 'udEmp1PayEnd',
            'emp1-reason': 'udEmp1Reason',
            'emp1-duties': 'udEmp1Duties',
            'emp2-name': 'PrevEmployer',
            'emp2-address': 'udEmp2Address',
            'emp2-city': 'udEmp2City',
            'emp2-state': 'udEmp2State',
            'emp2-zip': 'udEmp2Zip',
            'emp2-phone': 'udEmp2Phone',
            'emp2-position': 'udEmp2Position',
            'emp2-date-from': 'udEmp2DateFrom',
            'emp2-date-to': 'udEmp2DateTo',
            'emp2-supervisor': 'udEmp2Supervisor',
            'emp2-pay-start': 'udEmp2PayStart',
            'emp2-pay-end': 'udEmp2PayEnd',
            'emp2-reason': 'udEmp2Reason',
            'emp2-duties': 'udEmp2Duties',
            'school-high-name': 'HighSchool',
            #                                'school-high-courses': 'xxx',
            #                                'school-high-completed': 'HSGradDate',
            #                                'school-high-degree': 'xxx',
            'school-college-name': 'College1',
            #                                'school-college-courses': 'xxx',
            'school-college-completed': 'udSchoolCollegeLastYear',
            'school-college-degree': 'College1Degree',
            'school-grad-name': 'College2',
            #                                'school-grad-courses': 'xxx',
            'school-grad-completed': 'udSchoolGradLastYear',
            'school-grad-degree': 'College2Degree',
            'school-other-name': 'udSchoolOtherName',
            #                                'school-other-courses': 'xxx',
            #                                'school-other-completed': 'xxx',
            'school-other-degree': 'udSchoolOtherDegree',
            #                                'skills': 'xxx',
            'guilty-more': 'udGuiltyMore',
            'military-branch': 'udmilitaryBranch',
            #                                'military-entered': 'xxx',
            'military-discharged': 'udVetDischargeDate',
            #                                'military-rank': 'xxx',
            #                                'military-duties': 'xxx',
            'dl-state': 'LicState',
            'dl-class': 'LicClass',
            'dl-number': 'LicNumber',
            'dl-expiration': 'udLicExpDate',
            'sex': 'Sex',
            'race': 'Race',
            'previous': 'udPrevious',
            'functions': 'udFunctions',
            'citizen': 'udCitizen',
            'emp1-contact': 'NoContactEmplYN',
            'emp2-contact': 'udEmp2Contact',
            #                                'emp3-contact': 'xxx',
            'school-high-graduate': 'udSchoolHighGraduate',
            'school-college-graduate': 'udSchoolCollegeGraduate',
            'school-grad-graduate': 'udSchoolGradGraduate',
            #                                'school-other-graduate': 'xxx',
            'guilty': 'udGuilty',
            'military': 'udMilitary',
            #                                'dl': 'xxx',
            #                                'Submitted Login': 'xxx',
            #                                'Submitted From': 'xxx',
        },
                       'HRAR': {
                           'TAGS': 'one-to-many',

                           'ref1-name': 'Name',
                           'ref1-company': 'udCompany',
                           #                                'ref1-address': 'xxx',
                           'ref1-phone': 'Phone',

                           'ref2-name': 'Name',
                           'ref2-company': 'udCompany',
                           #                                'ref2-address': 'xxx',
                           'ref2-phone': 'Phone',

                           'ref3-name': 'Name',
                           'ref3-company': 'udCompany',
                           #                                'ref3-address': 'xxx',
                           'ref3-phone': 'Phone',
                       }
        }
