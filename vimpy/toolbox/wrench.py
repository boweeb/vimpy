# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Feb 15, 2013

@author: Jesse Butcher

toolbox.wrench:
    This tool contains general functions with application-wide utility.
"""

import hashlib
import types

from vimpy import G


###############################################################################
# FUNCTIONS
###############################################################################
def report_to_console(parent_form, description, var_to_report):
    """Prints variable values to both console and console window"""
    control = parent_form.txtOutput
    d = description
    v = var_to_report
    print d + ": " + v
    control.append(d + ": " + v)
    vbar = control.verticalScrollBar()
    vbar.setValue(vbar.maximum())


def hash_file(afile, hasher=hashlib.sha512(), blocksize=2 ** 21):
    """Compute SHA512 digest of input file. Return hexdigest."""
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def nullz(check_string):
    return (lambda: check_string if check_string is not None else "")()


def sanitize_uni(parent_form, value):
    """
    'value' is unicode erroneously trapped in a 'str' type. Reassigning 'value'
    as a 'utf-8' so it can be read properly. This is caused by the webform
    accepting unicode data into a MySQL table with a latin1 charset.
    """
    if value is None:
        return None

#     print "\tType: {}    Value: {}".format(type(val), val)
    ut8 = unicode(value, 'utf-8', 'ignore')
    asc = ut8.encode('ascii', 'ignore')

    # This is defined in the jig now so it's only ever compiled once.
#     control_chars = ''.join(map(unichr, range(0,32) + range(127,160)))
#     control_char_re = re.compile('[%s]' % re.escape(control_chars))
    
    output = G.control_char_re.sub('#', asc)
    
    if output != value:
        report_to_console(parent_form,
                          "\tWARNING",
                          "Replacing unicode chars with '#'s: {}".format(output))

    return output

