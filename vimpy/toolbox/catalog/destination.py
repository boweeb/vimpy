# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Mar 11, 2013

@author: Jesse Butcher

toolbox.catalog.destination:
    This tools contains class models of the destination tables.

  ! I'm suspending line-wrapping policies for the column definitions.

    To generate ("reflect") model code, ref:
        http://turbogears.org/2.1/docs/main/Utilities/sqlautocode.html
        To use correct versions, I created a virtualenv:
            ~/workspace/sac
        Just do the following:
            cd ~/workspace/sac
            source bin/activate
            sqlautocode -d -o <outfile.py> -e -t <table. <conn. str.>
            deactivate
        The -d flag (declarative) sometimes crashes it and can
            be omitted for the classic style table def.
        The -e flag generates example code at the end. Not req'd.
        Many fields will generate with these attributes:
                convert_unicode=False
                assert_unicode=None
                unicode_error=None
                _warn_on_bytestring=False
            They may be trimmed as all those values are defaults anyway.

"""

from sqlalchemy import Column, Sequence, ForeignKey
from sqlalchemy.types import Integer, String, Numeric
from sqlalchemy.orm import relationship, backref

from sqlalchemy.dialects.mssql import (SMALLDATETIME,
                                       VARCHAR, CHAR,
                                       UNIQUEIDENTIFIER)

from vimpy.toolbox.catalog import DstBase

__all__ = ['Resource', 'Reference']


class Resource(DstBase):
    """ HR Resource """
    __tablename__ = "HRRM"
    
    # Commented lines: exist in db but aren't used by the map or required.
    HRCo = Column(Integer, default=1, nullable=False)
    HRRef = Column(Integer, nullable=False)
    PRCo = Column(Integer, default=1)
#     PREmp = Column(Integer)
    LastName = Column(String(30), nullable=False)
    FirstName = Column(String(30))
#     MiddleName = Column(String(15))
    SortName = Column(String(15), nullable=False)
    Address = Column(String(60))
    City = Column(String(30))
    State = Column(String(4))
    Zip = Column(String(12))
#     Address2 = Column(String(60))
    Phone = Column(String(20))
#     WorkPhone = Column(String(20))
#     Pager = Column(String(20))
    CellPhone = Column(String(20))
#     SSN = Column(String(11))
    Sex = Column(String(1), nullable=False, default='M')
    Race = Column(String(2))
#     BirthDate = Column(SMALLDATETIME)
#     HireDate = Column(SMALLDATETIME)
#     TermDate = Column(SMALLDATETIME)
#     TermReason = Column(String(20))
    ActiveYN = Column(String(1), default='N', nullable=False)
#     Status = Column(String(10))
#     PRGroup = Column(Integer)
#     PRDept = Column(String(10))
#     StdCraft = Column(String(10))
#     StdClass = Column(String(10))
#     StdInsCode = Column(String(10))
#     StdTaxState = Column(String(4))
#     StdUnempState = Column(String(4))
#     StdInsState = Column(String(4))
#     StdLocal = Column(String(10))
    W4CompleteYN = Column(String(1), nullable=False, default='N')
#     PositionCode = Column(String(10))
    NoRehireYN = Column(String(1), nullable=False, default='N')
#     MaritalStatus = Column(String(1))
#     MaidenName = Column(String(20))
#     SpouseName = Column(String(30))
    PassPort = Column(String(1), nullable=False, default='N')
    RelativesYN = Column(String(1), nullable=False, default='N')
    HandicapYN = Column(String(1), nullable=False, default='N')
#     HandicapDesc = Column(String(30))
#     VetJobCategory = Column(String(2))
    PhysicalYN = Column(String(1), nullable=False, default='N')
#     PhysDate = Column(SMALLDATETIME)
#     PhysExpireDate = Column(SMALLDATETIME)
#     PhysResults = Column(String)
    LicNumber = Column(String(20))
#     LicType = Column(String(20))
    LicState = Column(String(4))
#     LicExpDate = Column(SMALLDATETIME)
    DriveCoVehiclesYN = Column(String(1), nullable=False, default='N')
#     I9Status = Column(String(20))
#     I9Citizen = Column(String(20))
#     I9ReviewDate = Column(SMALLDATETIME)
#     TrainingBudget = Column(Numeric(precision=12, scale=2))
#     CafeteriaPlanBudget = Column(Numeric(precision=12, scale=2))
    HighSchool = Column(String(30))
#     HSGradDate = Column(SMALLDATETIME)
    College1 = Column(String(30))
#     College1BegDate = Column(SMALLDATETIME)
#     College1EndDate = Column(SMALLDATETIME)
    College1Degree = Column(String(20))
    College2 = Column(String(30))
#     College2BegDate = Column(SMALLDATETIME)
#     College2EndDate = Column(SMALLDATETIME)
    College2Degree = Column(String(20))
#     ApplicationDate = Column(SMALLDATETIME)
#     AvailableDate = Column(SMALLDATETIME)
#     LastContactDate = Column(SMALLDATETIME)
#     ContactPhone = Column(String(20))
#     AltContactPhone = Column(String(20))
#     ExpectedSalary = Column(Numeric(precision=12, scale=2))
    Source = Column(String(30))
#     SourceCost = Column(Numeric(precision=12, scale=2))
    CurrentEmployer = Column(String(30))
#     CurrentTime = Column(String(20))
    PrevEmployer = Column(String(30))
#     PrevTime = Column(String(20))
    NoContactEmplYN = Column(String(1), nullable=False, default='N')
#     HistSeq = Column(Integer)
#     Notes = Column(String)
    ExistsInPR = Column(String(1), nullable=False, default='N')
#     EarnCode = Column(Integer)
#     PhotoName = Column(String(255))
    UniqueAttchID = Column(Integer)
    TempWorker = Column(String(1), nullable=False, default='N')
    Email = Column(String(60))
#     Suffix = Column(String(4))
#     DisabledVetYN = Column(String(1), default='N')
#     VietnamVetYN = Column(String(1), default='N')
#     OtherVetYN = Column(String(1), default='N')
#     VetDischargeDate = Column(SMALLDATETIME)
#     OccupCat = Column(String(10))
#     CatStatus = Column(String(1))
    LicClass = Column(String(1))
#     DOLHireState = Column(String(4))
    NonResAlienYN = Column(String(1), nullable=False, default='N')
    KeyID = Column(Integer,
                   Sequence('seqRM', 1, 1),
                   primary_key=True,
                   nullable=False)
#     Country = Column(String(2))
#     LicCountry = Column(String(2))
#     OTOpt = Column(String(1))
#     OTSched = Column(Integer)
#     Shift = Column(Integer)
#     PTOAppvrGrp = Column(Integer)
#     HDAmt = Column(Numeric(precision=12, scale=2))
#     F1Amt = Column(Numeric(precision=12, scale=2))
#     LCFStock = Column(Numeric(precision=12, scale=2))
#     LCPStock = Column(Numeric(precision=12, scale=2))
    AFServiceMedalVetYN = Column(String(1), nullable=False, default='N')
#     WOTaxState = Column(String(4))
#     WOLocalCode = Column(String(10))
    # Custom Fields:
#     udNextel = Column(String(14))
#     udDID = Column(String(14))
#     udExt = Column(String(3))
#     udFaxmaker = Column(String(14))
#     udHasSetterlinEmail = Column(String(1), default='N')
    udRelativesMore = Column(String(255))
    udEmp1Address = Column(String(255))
    udEmp1City = Column(String(255))
    udEmp1State = Column(String(255))
    udEmp1Zip = Column(String(255))
    udEmp1Phone = Column(String(255))
    udEmp1Position = Column(String(255))
    udEmp1DateFrom = Column(String(255))
    udEmp1DateTo = Column(String(255))
    udEmp1Supervisor = Column(String(255))
    udEmp1PayStart = Column(String(255))
    udEmp1PayEnd = Column(String(255))
    udEmp1Reason = Column(String(255))
    udEmp1Duties = Column(String(255))
    udEmp2Address = Column(String(255))
    udEmp2City = Column(String(255))
    udEmp2State = Column(String(255))
    udEmp2Zip = Column(String(255))
    udEmp2Phone = Column(String(255))
    udEmp2Position = Column(String(255))
    udEmp2DateFrom = Column(String(255))
    udEmp2DateTo = Column(String(255))
    udEmp2Supervisor = Column(String(255))
    udEmp2PayStart = Column(String(255))
    udEmp2PayEnd = Column(String(255))
    udEmp2Reason = Column(String(255))
    udEmp2Duties = Column(String(255))
    udPrevious = Column(String(1), default='N')
    udFunctions = Column(String(1), default='N')
    udCitizen = Column(String(1), default='Y')
    udEmp2Contact = Column(String(1), default='N')
    udSchoolHighGraduate = Column(String(1), default='N')
    udSchoolCollegeGraduate = Column(String(1), default='N')
    udSchoolGradGraduate = Column(String(1), default='N')
    udSchoolOtherName = Column(String(255))
    udSchoolOtherDegree = Column(String(255))
    udMilitary = Column(String(1), default='N')
    udmilitaryBranch = Column(String(255))
    udGuilty = Column(String(1), default='N')
    udGuiltyMore = Column(String(255))
    udAppPosition = Column(String(255))
    udWebPKID = Column(Numeric(precision=14, scale=4))
    udSchoolCollegeLastYear = Column(String(255))
    udSchoolGradLastYear = Column(String(255))
    udLicExpDate = Column(String(255))
    udAvailableDate = Column(String(255))
    udVetDischargeDate = Column(String(255))
    udExpectedSalary = Column(String(255))

    #relation definitions
    # references = relationship('Reference', backref="applicant")

    def __str__(self):
        return "HR Resource: {}, Name: \"{} {}\"".format(self.udWebPKID,
                                                         self.FirstName,
                                                         self.LastName)

    def __repr__(self):
        return "<Resource('{}', '{}', '{}')>".format(self.udWebPKID,
                                                     self.HRRef,
                                                     self.LastName)


class Reference(DstBase):
    """ HR Application Reference """
    __tablename__ = "HRAR"

    KeyID = Column('KeyID',
                   Integer(),
                   primary_key=True,
                   nullable=False,
                   default=Sequence('KeyID_identity',
                                    start=1,
                                    increment=1,
                                    optional=False))
    HRCo = Column('HRCo',                   Integer(),         nullable=False, default=1)
#     HRRef         = Column('HRRef',        Integer(),         nullable=False)
    HRRef = Column('HRRef',                 Integer(), ForeignKey('HRRM.HRRef'), nullable=False)
    Seq = Column('Seq',                     Integer(),         nullable=False)
    Name = Column('Name',                   String(length=30), nullable=False)
    Phone = Column('Phone',                 String(length=20))
    RefNotes = Column('RefNotes',           String(length=None))
    UniqueAttchID = Column('UniqueAttchID', Integer())
    udCompany = Column(String(255))

    resource = relationship("Resource",
                            primaryjoin='Resource.HRRef == Reference.HRRef',
                            backref=backref('references'))

    def __str__(self):
        return "Applicant Reference: Name: \"{} {}\"".format(self.Name)

    def __repr__(self):
        return "<AppRef('{}')>".format(self.Name)


# class Attachment(DstBase):
#     """ HQ Attachment, read "Resume" """
#     __tablename__ = "HQAT"
# 
#     HQCo             = Column('HQCo',             Integer(),           nullable=False)
#     FormName         = Column('FormName',         VARCHAR(length=30),  nullable=False, default='HRResourceMaster')
#     KeyField         = Column('KeyField',         VARCHAR(length=500), nullable=False)
#     Description      = Column('Description',      VARCHAR(length=255))
#     AddedBy          = Column('AddedBy',          VARCHAR(length=128),                 default='kjackson')
#     AddDate          = Column('AddDate',          SMALLDATETIME())
#     DocName          = Column('DocName',          VARCHAR(length=512))
#     AttachmentID     = Column('AttachmentID',     Integer(), nullable=False, primary_key=True)
#     TableName        = Column('TableName',        VARCHAR(length=128),                 default='HRRM')
#     UniqueAttchID    = Column('UniqueAttchID',    UNIQUEIDENTIFIER())
#     OrigFileName     = Column('OrigFileName',     VARCHAR(length=512))
#     DocAttchYN       = Column('DocAttchYN',       CHAR(length=1),      nullable=False, default='N')
#     CurrentState     = Column('CurrentState',     CHAR(length=1),      nullable=False, default='A')
#     AttachmentTypeID = Column('AttachmentTypeID', Integer(),                           default=7)
#     IsEmail          = Column('IsEmail',          CHAR(length=1),      nullable=False, default='N')
# 
#     KeyID = Column('KeyID',
#                    Integer(),
#                    primary_key=True,
#                    nullable=False,
#                    default=Sequence('KeyID_identity',
#                                       start=1,
#                                       increment=1,
#                                       optional=False))
# 
# #     resource = relationship("Resource",
# #                              primaryjoin='Resource.UniqueAttchID == Reference.UniqueAttchID',
# #                              backref=backref('attachments'))
# 
#     def __str__(self):
#         return "Applicant Resume. ID: {}, File Name: {}".format(self.AttachmentID, self.OrigFileName)
# 
#     def __repr__(self):
#         return "<Attachment('{}', '{}')>".format(self.AttachmentID, self.OrigFileName)
