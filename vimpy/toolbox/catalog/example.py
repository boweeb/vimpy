# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 26, 2013

@author: Jesse Butcher

toolbox.catalog.example:
    This is an example model for various interactive tests.

'''

from sqlalchemy import Column, Sequence, ForeignKey
from sqlalchemy.types import Integer, String
from sqlalchemy.orm import relationship, backref

#Note: The TurboGears/Pylons example implied the next line
#  but 'metadata' and 'DBSession' don't seem to be needed here.
#from vimpy.toolbox.catalog import DeclarativeBase, metadata, DBSession
from vimpy.toolbox.catalog import DstBase

__all__ = ['Car', 'Person']


class Car(DstBase):
    """ Test model for unittests and other testing. """
    __tablename__ = "cars"

    pkid = Column('ID', Integer(),
                  primary_key=True,
                  nullable=False,
                  default=Sequence('KeyID_identity',
                                     start=1,
                                     increment=1,
                                     optional=False))
    make = Column('Make', String(), nullable=False)
    modl = Column('Model', String(), nullable=True)
    year = Column('Year', Integer(), nullable=False)
    colr = Column('Color', String(length=30), nullable=True)
    psid = Column('Person', Integer(), ForeignKey('people.ID'))

    owner = relationship('Person', backref=backref('cars'))

    # Pretty print -- set blank str default for nullable fields
    def __str__(self):
        p_colr = "" if self.colr is None else self.colr
        p_modl = "" if self.modl is None else self.modl
        props = [p_colr, self.year, self.make, p_modl]

        return "Car -- {} {} {} {}".format(*props)

    def __repr__(self):
        return "<Car('{0}', '{1}')>".format(self.year, self.make)


class Person(DstBase):
    """ Test model for unittests and other testing. """
    __tablename__ = "people"

    pkid = Column('ID', Integer(),
                  primary_key=True,
                  nullable=False,
                  default=Sequence('KeyID_identity',
                                     start=1,
                                     increment=1,
                                     optional=False))
    nam = Column('Name', String(), nullable=False)
    age = Column('Age', Integer(), nullable=True)
    gen = Column('Gender', String(1), nullable=False)

    # Pretty print -- set blank str default for nullable fields
    def __str__(self):
        p_age = "" if self.age is None else self.age
        props = [self.nam, p_age, self.gen]

        return "Person -- {} {} {}".format(*props)

    def __repr__(self):
        return "<Person('{}, {}')>".format(self.nam, self.gen)
