# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 26, 2013

@author: Jesse Butcher

toolbox.catalog:
    This tool is the touchpoint for using SQLAlchemy. Includes models.
    Inspired by TurboGears2.2 example project.

'''

import cStringIO
from sqlalchemy import create_engine, exc
from sqlalchemy.orm import scoped_session, sessionmaker
#from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

from vimpy import G
from vimpy.toolbox.wrench import report_to_console as rpt
from vimpy.toolbox.screwdriver import cfg_warnbox, gen_errorbox

from keyczar.errors import KeyNotFoundError, Base64DecodingError


# Global session registry:
session_factory = sessionmaker(autoflush=True, autocommit=False)
Session = scoped_session(session_factory)

# Base class for all of our model classes.
# Defined with SQLAlchemy's declarative extension.
DstBase = declarative_base()
SrcBase = declarative_base()

# Global metadata.
# The default metadata is the one from the declarative base.
dst_metadata = DstBase.metadata
src_metadata = SrcBase.metadata

# Import models.
from vimpy.toolbox.catalog.source import *
from vimpy.toolbox.catalog.destination import *
from vimpy.toolbox.catalog.example import *


#############################################################################
# Define engine

def init_engine(name, pw_fld):
    """ Create destination engine. """
    if (pw_fld != ""):
        try:
            buff = cStringIO.StringIO(G.crypter.Decrypt(pw_fld))
        except KeyNotFoundError:
            print "Keyczar Error: Key not found."
            cfg_warnbox('Warning',
                        "Either a database password hasn't been saved or" +
                        " the key hashes don't match.")
            return False
        except Base64DecodingError:
            print "Keyczar Error: Could not decode."
            cfg_warnbox('Error',
                        "The password value in the setting.cfg file could " +
                        "not be decoded by the key-handler, keyczar.")
            return False

        try:
            print "Evaluating name..."
            if (name == 'dst_engine'):
                print "    Init dst_engine..."
                cx_str = ('mssql://' + G.dst_user +
                              ':' + buff.read() +
                              '@' + G.dst_dsn
                              )
                dst_engine = create_engine(cx_str
                                           , implicit_returning = False
#                                             , echo=True
                                           )
                buff.close()
                print "        ... Success"
                return dst_engine
            elif (name == 'src_engine'):
                print "    Init src_engine..."
                cx_str = ('mysql://' + G.src_user +
                              ':' + buff.read() +
                              '@' + G.src_host +
                              ':' + G.src_port +
                              '/' + G.src_schema
#                               + '?charset=utf8'
                              )
#                 print cx_str
                src_engine = create_engine(cx_str
#                                            ,echo=True
                                           )
                buff.close()
                print "        ... Success"
                return src_engine
            else:
                print "Invalid engine name."
                buff.close()
                return False

        except exc.SQLAlchemyError, e:
            print "SQLAlchemyError:: %s:" % e.args[0]
            return False
        except NameError, e:
            print "NameError: {}".format(e)
            return False
        except ValueError, e:
            msg = ("There's something wrong with the database" +
                   " configuration. Likely the 'port' setting is wrong.")
            cfg_warnbox("Error", msg)
            return False

    else:
#         raise ValueError("{} password can't be blank.".format(name))
        msg = ("The password for engine \"{}\" is blank.\n".format(name) +
               "Would you like to open the Options dialog now to re-save " +
               "the database passwords?")
        cfg_warnbox("Warning", msg)
        return False


#############################################################################
# Initialize source and destination engines.

def init_all_engines():
    """ Initialize engines. """
    src_engine = init_engine('src_engine', G.src_pass)
    if (src_engine == False):
        print "Source engine failed to initialize."
        return False

    dst_engine = init_engine('dst_engine', G.dst_pass)
    if (dst_engine == False):
        print "Source engine failed to initialize."
        return False

    Session.configure(binds={Element: src_engine,
                             Resource: dst_engine,
                             Reference: dst_engine})
    return True


#############################################################################
# Set global status of the engines being ready.

G.engines_ready = init_all_engines()

if (G.engines_ready):
    print "Engines ready."
else:
    print "Engines not ready." 
