# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 11, 2013

@author: Jesse Butcher

toolbox.catalog:
    This tool contains table definitions for SQLAlchemy.

'''

from sqlalchemy import Column, Sequence, ForeignKey
from sqlalchemy.types import Integer, String
from sqlalchemy.orm import relationship, backref

from vimpy.toolbox.catalog import DeclarativeBase, metadata, DBSession

__all__ = ['Reference']


class Reference(DeclarativeBase):
    """ HR Application References """
    __tablename__ = "HRAR"

    HRCo = Column('HRCo', Integer(), nullable=False, default=1)
#     HRRef = Column('HRRef', Integer(), nullable=False)
    HRRef = Column('HRRef', Integer(), ForeignKey('HRRM.HRRef'), nullable=False)
    Seq = Column('Seq', Integer(), nullable=False)
    Name = Column('Name', String(length=30,
                                 convert_unicode=False,
                                 _warn_on_bytestring=False
                                 ), nullable=False)
    Phone = Column('Phone', String(length=20,
                                   convert_unicode=False,
                                   _warn_on_bytestring=False))
    RefNotes = Column('RefNotes', String(length=None,
                                         convert_unicode=False,
                                         _warn_on_bytestring=False))
    UniqueAttchID = Column('UniqueAttchID', Integer())
    KeyID = Column('KeyID',
                   Integer(),
                   primary_key=True,
                   nullable=False,
                   default=Sequence('KeyID_identity',
                                      start=1,
                                      increment=1,
                                      optional=False))
    
    applicant = relationship("Applicant",
                             primaryjoin='Applicant.HRRef == Reference.HRRef',
                             backref=backref('references'))

    def __str__(self):
        return "Applicant Reference. KeyID: {}, Name: {}".format(self.KeyID,
                                                                 self.Name)

    def __repr__(self):
        return "<AppRef('{}', '{}')>".format(self.KeyID, self.Name)


#    applicant = relationship('Applicant', backref=backref('references'))
