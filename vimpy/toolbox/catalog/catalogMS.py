# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 11, 2013

@author: Jesse Butcher

toolbox.catalog:
    This tool contains table definitions for SQLAlchemy.

'''
#from toolbox import jig

#from sqlalchemy import create_engine
#from sqlalchemy import ForeignKey 
from sqlalchemy import Column, Sequence, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, \
                           scoped_session, sessionmaker

from sqlalchemy.dialects.mssql import \
    BIGINT, CHAR, INTEGER, NUMERIC, \
    SMALLINT, UNIQUEIDENTIFIER, VARCHAR

# Instantiate my globals
#g = jig.GlobalSwatch()

#~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~#
#engine = create_engine('sqlite:///' + g.s_full_, convert_unicode=True)
#engine = create_engine('mssql://'+g.m_user_+':'+g.m_pass_+'@'+g.m_dsn_)
#db_session = scoped_session(sessionmaker(autocommit=False,
#                                         autoflush=False,
#                                         bind=engine))
Base = declarative_base()
#Base.query = db_session.query_property()
#~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~##~~#


class Applicant(Base):
    """ HR Resource Master """
    __tablename__ = "HRRM"

    HRCo = Column(u'HRCo', Integer(), nullable=False)
    HRRef = Column(u'HRRef', INTEGER(), nullable=False)
    PRCo = Column(u'PRCo', Integer())
    PREmp = Column(u'PREmp', INTEGER())
    LastName = Column(u'LastName', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    FirstName = Column(u'FirstName', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    MiddleName = Column(u'MiddleName', VARCHAR(length=15, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    SortName = Column(u'SortName', VARCHAR(length=15, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    Address = Column(u'Address', VARCHAR(length=60, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    City = Column(u'City', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    State = Column(u'State', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Zip = Column(u'Zip', VARCHAR(length=12, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Address2 = Column(u'Address2', VARCHAR(length=60, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Phone = Column(u'Phone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    WorkPhone = Column(u'WorkPhone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Pager = Column(u'Pager', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    CellPhone = Column(u'CellPhone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    SSN = Column(u'SSN', CHAR(length=11, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Sex = Column(u'Sex', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    Race = Column(u'Race', CHAR(length=2, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    BirthDate = Column(u'BirthDate', DateTime(timezone=False))
    HireDate = Column(u'HireDate', DateTime(timezone=False))
    TermDate = Column(u'TermDate', DateTime(timezone=False))
    TermReason = Column(u'TermReason', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    ActiveYN = Column(u'ActiveYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    Status = Column(u'Status', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    PRGroup = Column(u'PRGroup', Integer())
    PRDept = Column(u'PRDept', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdCraft = Column(u'StdCraft', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdClass = Column(u'StdClass', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdInsCode = Column(u'StdInsCode', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdTaxState = Column(u'StdTaxState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdUnempState = Column(u'StdUnempState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdInsState = Column(u'StdInsState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    StdLocal = Column(u'StdLocal', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    W4CompleteYN = Column(u'W4CompleteYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    PositionCode = Column(u'PositionCode', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    NoRehireYN = Column(u'NoRehireYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    MaritalStatus = Column(u'MaritalStatus', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    MaidenName = Column(u'MaidenName', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    SpouseName = Column(u'SpouseName', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    PassPort = Column(u'PassPort', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    RelativesYN = Column(u'RelativesYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    HandicapYN = Column(u'HandicapYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    HandicapDesc = Column(u'HandicapDesc', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    VetJobCategory = Column(u'VetJobCategory', VARCHAR(length=2, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    PhysicalYN = Column(u'PhysicalYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    PhysDate = Column(u'PhysDate', DateTime(timezone=False))
    PhysExpireDate = Column(u'PhysExpireDate', DateTime(timezone=False))
    PhysResults = Column(u'PhysResults', VARCHAR(length=None, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicNumber = Column(u'LicNumber', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicType = Column(u'LicType', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicState = Column(u'LicState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicExpDate = Column(u'LicExpDate', DateTime(timezone=False))
    DriveCoVehiclesYN = Column(u'DriveCoVehiclesYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    I9Status = Column(u'I9Status', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    I9Citizen = Column(u'I9Citizen', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    I9ReviewDate = Column(u'I9ReviewDate', DateTime(timezone=False))
    TrainingBudget = Column(u'TrainingBudget', NUMERIC(precision=12, scale=2, asdecimal=True))
    CafeteriaPlanBudget = Column(u'CafeteriaPlanBudget', NUMERIC(precision=12, scale=2, asdecimal=True))
    HighSchool = Column(u'HighSchool', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    HSGradDate = Column(u'HSGradDate', DateTime(timezone=False))
    College1 = Column(u'College1', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    College1BegDate = Column(u'College1BegDate', DateTime(timezone=False))
    College1EndDate = Column(u'College1EndDate', DateTime(timezone=False))
    College1Degree = Column(u'College1Degree', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    College2 = Column(u'College2', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    College2BegDate = Column(u'College2BegDate', DateTime(timezone=False))
    College2EndDate = Column(u'College2EndDate', DateTime(timezone=False))
    College2Degree = Column(u'College2Degree', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    ApplicationDate = Column(u'ApplicationDate', DateTime(timezone=False))
    AvailableDate = Column(u'AvailableDate', DateTime(timezone=False))
    LastContactDate = Column(u'LastContactDate', DateTime(timezone=False))
    ContactPhone = Column(u'ContactPhone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    AltContactPhone = Column(u'AltContactPhone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    ExpectedSalary = Column(u'ExpectedSalary', NUMERIC(precision=12, scale=2, asdecimal=True))
    Source = Column(u'Source', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    SourceCost = Column(u'SourceCost', NUMERIC(precision=12, scale=2, asdecimal=True))
    CurrentEmployer = Column(u'CurrentEmployer', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    CurrentTime = Column(u'CurrentTime', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    PrevEmployer = Column(u'PrevEmployer', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    PrevTime = Column(u'PrevTime', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    NoContactEmplYN = Column(u'NoContactEmplYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    HistSeq = Column(u'HistSeq', INTEGER())
    Notes = Column(u'Notes', VARCHAR(length=None, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    ExistsInPR = Column(u'ExistsInPR', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    EarnCode = Column(u'EarnCode', SMALLINT())
    PhotoName = Column(u'PhotoName', VARCHAR(length=255, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    UniqueAttchID = Column(u'UniqueAttchID', UNIQUEIDENTIFIER())
    TempWorker = Column(u'TempWorker', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    Email = Column(u'Email', VARCHAR(length=60, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    Suffix = Column(u'Suffix', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    DisabledVetYN = Column(u'DisabledVetYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    VietnamVetYN = Column(u'VietnamVetYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    OtherVetYN = Column(u'OtherVetYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    VetDischargeDate = Column(u'VetDischargeDate', DateTime(timezone=False))
    OccupCat = Column(u'OccupCat', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    CatStatus = Column(u'CatStatus', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicClass = Column(u'LicClass', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    DOLHireState = Column(u'DOLHireState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    NonResAlienYN = Column(u'NonResAlienYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    KeyID = Column(u'KeyID', BIGINT(), primary_key=True, nullable=False, default=Sequence(u'KeyID_identity', start=1, increment=1, optional=False))
    Country = Column(u'Country', CHAR(length=2, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    LicCountry = Column(u'LicCountry', CHAR(length=2, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    OTOpt = Column(u'OTOpt', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    OTSched = Column(u'OTSched', Integer())
    Shift = Column(u'Shift', Integer())
    PTOAppvrGrp = Column(u'PTOAppvrGrp', Integer())
    HDAmt = Column(u'HDAmt', NUMERIC(precision=12, scale=2, asdecimal=True))
    F1Amt = Column(u'F1Amt', NUMERIC(precision=12, scale=2, asdecimal=True))
    LCFStock = Column(u'LCFStock', NUMERIC(precision=12, scale=2, asdecimal=True))
    LCPStock = Column(u'LCPStock', NUMERIC(precision=12, scale=2, asdecimal=True))
    AFServiceMedalVetYN = Column(u'AFServiceMedalVetYN', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    WOTaxState = Column(u'WOTaxState', VARCHAR(length=4, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    WOLocalCode = Column(u'WOLocalCode', VARCHAR(length=10, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    udNextel = Column(u'udNextel', VARCHAR(length=14, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    udDID = Column(u'udDID', VARCHAR(length=14, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    udExt = Column(u'udExt', VARCHAR(length=3, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    udFaxmaker = Column(u'udFaxmaker', VARCHAR(length=14, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    udHasSetterlinEmail = Column(u'udHasSetterlinEmail', CHAR(length=1, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))

    #relation definitions
#    references = relationship('Reference', backref="applicant")


###############################################################################


class Reference(Base):
    """ HR Application References """
    __tablename__ = "HRAR"

    HRCo = Column(u'HRCo', Integer(), nullable=False)
    HRRef = Column(u'HRRef', INTEGER(), nullable=False)
#    HRRef = Column(u'HRRef', INTEGER(), ForeignKey('HRRM.HRRef'), nullable=False)
    Seq = Column(u'Seq', INTEGER(), nullable=False)
    Name = Column(u'Name', VARCHAR(length=30, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False), nullable=False)
    Phone = Column(u'Phone', VARCHAR(length=20, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    RefNotes = Column(u'RefNotes', VARCHAR(length=None, convert_unicode=False, assert_unicode=None, unicode_error=None, _warn_on_bytestring=False))
    UniqueAttchID = Column(u'UniqueAttchID', UNIQUEIDENTIFIER())
    KeyID = Column(u'KeyID', BIGINT(), primary_key=True, nullable=False, default=Sequence(u'KeyID_identity', start=1, increment=1, optional=False))
    
#    applicant = relationship('Applicant', backref=backref('references'))
