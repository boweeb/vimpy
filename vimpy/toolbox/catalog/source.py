# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Created on Apr 5, 2013

@author: Jesse Butcher

toolbox.catalog.source:
    This tools contains class models of the source tables.

"""

from sqlalchemy import Column
from sqlalchemy.dialects.mysql import (DECIMAL, VARCHAR, INTEGER,
                                       LONGBLOB, LONGTEXT)

from vimpy.toolbox.catalog import SrcBase

__all__ = ['Element']


class Element(SrcBase):
    """ A propaeduetical record. Several are needed to create an Applicant. """
    __tablename__ = "wp_cf7dbplugin_submits"

    submit_time = Column(DECIMAL(precision=16, scale=4, asdecimal=True),
                         primary_key=True,
                         nullable=False)
    form_name = Column(VARCHAR(length=127))
    field_name = Column(VARCHAR(length=127))
    field_value = Column(LONGTEXT())
    field_order = Column(INTEGER(), primary_key=True)
    file = Column(LONGBLOB(length=None))

    # Pretty print
    def __str__(self):
        props = [self.submit_time, self.field_name, self.field_value]
        return "Element -- ({})  {}:{} ".format(*props)
 
    def __repr__(self):
        props = [self.submit_time, self.field_name, self.field_value]
        return "<Element('{}', '{}', '{}')>".format(*props)
