USE [Viewpoint]
GO
/****** Object:  Table [dbo].[bHRRM]    Script Date: 03/01/2013 16:08:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bHRRM](
	[HRCo] [dbo].[bCompany] NOT NULL,
	[HRRef] [dbo].[bHRRef] NOT NULL,
	[PRCo] [dbo].[bCompany] NULL,
	[PREmp] [dbo].[bEmployee] NULL,
	[LastName] [varchar](30) NOT NULL,
	[FirstName] [varchar](30) NULL,
	[MiddleName] [varchar](15) NULL,
	[SortName] [varchar](15) NOT NULL,
	[Address] [varchar](60) NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](4) NULL,
	[Zip] [dbo].[bZip] NULL,
	[Address2] [varchar](60) NULL,
	[Phone] [dbo].[bPhone] NULL,
	[WorkPhone] [dbo].[bPhone] NULL,
	[Pager] [dbo].[bPhone] NULL,
	[CellPhone] [dbo].[bPhone] NULL,
	[SSN] [char](11) NULL,
	[Sex] [char](1) NOT NULL,
	[Race] [char](2) NULL,
	[BirthDate] [smalldatetime] NULL,
	[HireDate] [dbo].[bDate] NULL,
	[TermDate] [dbo].[bDate] NULL,
	[TermReason] [varchar](20) NULL,
	[ActiveYN] [dbo].[bYN] NOT NULL,
	[Status] [varchar](10) NULL,
	[PRGroup] [dbo].[bGroup] NULL,
	[PRDept] [dbo].[bDept] NULL,
	[StdCraft] [dbo].[bCraft] NULL,
	[StdClass] [dbo].[bClass] NULL,
	[StdInsCode] [dbo].[bInsCode] NULL,
	[StdTaxState] [varchar](4) NULL,
	[StdUnempState] [varchar](4) NULL,
	[StdInsState] [varchar](4) NULL,
	[StdLocal] [dbo].[bLocalCode] NULL,
	[W4CompleteYN] [dbo].[bYN] NOT NULL,
	[PositionCode] [varchar](10) NULL,
	[NoRehireYN] [dbo].[bYN] NOT NULL,
	[MaritalStatus] [char](1) NULL,
	[MaidenName] [varchar](20) NULL,
	[SpouseName] [varchar](30) NULL,
	[PassPort] [dbo].[bYN] NOT NULL,
	[RelativesYN] [dbo].[bYN] NOT NULL,
	[HandicapYN] [dbo].[bYN] NOT NULL,
	[HandicapDesc] [dbo].[bDesc] NULL,
	[VetJobCategory] [varchar](2) NULL,
	[PhysicalYN] [dbo].[bYN] NOT NULL,
	[PhysDate] [dbo].[bDate] NULL,
	[PhysExpireDate] [dbo].[bDate] NULL,
	[PhysResults] [varchar](max) NULL,
	[LicNumber] [varchar](20) NULL,
	[LicType] [varchar](20) NULL,
	[LicState] [varchar](4) NULL,
	[LicExpDate] [dbo].[bDate] NULL,
	[DriveCoVehiclesYN] [dbo].[bYN] NOT NULL,
	[I9Status] [varchar](20) NULL,
	[I9Citizen] [varchar](20) NULL,
	[I9ReviewDate] [dbo].[bDate] NULL,
	[TrainingBudget] [dbo].[bDollar] NULL,
	[CafeteriaPlanBudget] [dbo].[bDollar] NULL,
	[HighSchool] [varchar](30) NULL,
	[HSGradDate] [dbo].[bDate] NULL,
	[College1] [varchar](30) NULL,
	[College1BegDate] [dbo].[bDate] NULL,
	[College1EndDate] [dbo].[bDate] NULL,
	[College1Degree] [varchar](20) NULL,
	[College2] [varchar](30) NULL,
	[College2BegDate] [dbo].[bDate] NULL,
	[College2EndDate] [dbo].[bDate] NULL,
	[College2Degree] [varchar](20) NULL,
	[ApplicationDate] [dbo].[bDate] NULL,
	[AvailableDate] [dbo].[bDate] NULL,
	[LastContactDate] [dbo].[bDate] NULL,
	[ContactPhone] [dbo].[bPhone] NULL,
	[AltContactPhone] [dbo].[bPhone] NULL,
	[ExpectedSalary] [dbo].[bDollar] NULL,
	[Source] [dbo].[bDesc] NULL,
	[SourceCost] [dbo].[bDollar] NULL,
	[CurrentEmployer] [dbo].[bDesc] NULL,
	[CurrentTime] [varchar](20) NULL,
	[PrevEmployer] [dbo].[bDesc] NULL,
	[PrevTime] [varchar](20) NULL,
	[NoContactEmplYN] [dbo].[bYN] NOT NULL,
	[HistSeq] [int] NULL,
	[Notes] [varchar](max) NULL,
	[ExistsInPR] [dbo].[bYN] NOT NULL,
	[EarnCode] [dbo].[bEDLCode] NULL,
	[PhotoName] [varchar](255) NULL,
	[UniqueAttchID] [uniqueidentifier] NULL,
	[TempWorker] [dbo].[bYN] NOT NULL CONSTRAINT [DF_bHRRM_TempWorker]  DEFAULT ('N'),
	[Email] [varchar](60) NULL,
	[Suffix] [varchar](4) NULL,
	[DisabledVetYN] [dbo].[bYN] NULL CONSTRAINT [DF_bHRRM_DisabledVetYN]  DEFAULT ('N'),
	[VietnamVetYN] [dbo].[bYN] NULL CONSTRAINT [DF_bHRRM_VietnamVetYN]  DEFAULT ('N'),
	[OtherVetYN] [dbo].[bYN] NULL CONSTRAINT [DF_bHRRM_OtherVetYN]  DEFAULT ('N'),
	[VetDischargeDate] [dbo].[bDate] NULL,
	[OccupCat] [varchar](10) NULL,
	[CatStatus] [char](1) NULL,
	[LicClass] [char](1) NULL,
	[DOLHireState] [varchar](4) NULL,
	[NonResAlienYN] [char](1) NOT NULL CONSTRAINT [DF_bHRRM_NonResAlienYN]  DEFAULT ('N'),
	[KeyID] [bigint] IDENTITY(1,1) NOT NULL,
	[Country] [char](2) NULL,
	[LicCountry] [char](2) NULL,
	[OTOpt] [char](1) NULL,
	[OTSched] [tinyint] NULL,
	[Shift] [tinyint] NULL,
	[PTOAppvrGrp] [dbo].[bGroup] NULL,
	[HDAmt] [dbo].[bDollar] NULL,
	[F1Amt] [dbo].[bDollar] NULL,
	[LCFStock] [dbo].[bDollar] NULL,
	[LCPStock] [dbo].[bDollar] NULL,
	[AFServiceMedalVetYN] [dbo].[bYN] NOT NULL CONSTRAINT [DF_bHRRM_AFServiceMedalVetYN]  DEFAULT ('N'),
	[WOTaxState] [varchar](4) NULL,
	[WOLocalCode] [dbo].[bLocalCode] NULL,
	[udNextel] [varchar](14) NULL,
	[udDID] [varchar](14) NULL,
	[udExt] [varchar](3) NULL,
	[udFaxmaker] [varchar](14) NULL,
	[udHasSetterlinEmail] [dbo].[bYN] NULL CONSTRAINT [DF__bHRRM__udHasSetterlinEmail__DEFAULT]  DEFAULT ('N'),
 CONSTRAINT [PK_bHRRM_KeyID] PRIMARY KEY NONCLUSTERED 
(
	[KeyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF