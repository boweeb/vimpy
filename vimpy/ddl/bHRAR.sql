USE [Viewpoint]
GO
/****** Object:  Table [dbo].[bHRAR]    Script Date: 03/01/2013 16:43:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bHRAR](
	[HRCo] [dbo].[bCompany] NOT NULL,
	[HRRef] [dbo].[bHRRef] NOT NULL,
	[Seq] [int] NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Phone] [dbo].[bPhone] NULL,
	[RefNotes] [varchar](max) NULL,
	[UniqueAttchID] [uniqueidentifier] NULL,
	[KeyID] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF