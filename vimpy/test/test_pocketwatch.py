# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Apr 10, 2013

@author: Jesse Butcher

test.pocketwatch:
    Unit tests for vimpy.toolbox.pocketwatch

'''

import unittest
import datetime
from vimpy.toolbox.pocketwatch import time2gen


###############################################################################
# GLOBALS
###############################################################################
# Simple
lst_simple = ['04/12/2011', '08/03/2012', '01/21/2013']
# MS SQLServer
lst_mssql = ['04/12/2011 08:43:21 AM', '08/03/2012 04:13:26 PM', '01/21/2013 02:05:56 AM']
# Epoch
lst_epoch = [1302612201, 1344024806, 1358751956]
# File Suffix
lst_filesfx = ['20110412', '20120803', '20130121']
# DateTime
lst_datetime = [datetime.datetime(2011, 4, 12, 8, 43, 21),
                datetime.datetime(2012, 8, 3, 16, 13, 26),
                datetime.datetime(2013, 1, 21, 2, 5, 56)]


###############################################################################
# TESTS
###############################################################################
class TestSimple(unittest.TestCase):
    """ Test suite for time2gen 'simple' generator. """

    def test_mssql(self):
        """ Convert to MS SQLServer timestamp """
        gen = time2gen(lst_simple, "simple", "mssql")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011 12:00:00 AM',
                           '08/03/2012 12:00:00 AM',
                           '01/21/2013 12:00:00 AM']

        self.assertEqual(result, expected_result)

    def test_epoch(self):
        """ Convert to Unix epoch integer """
        gen = time2gen(lst_simple, "simple", "epoch")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['1302580800',
                           '1343966400',
                           '1358744400']

        self.assertEqual(result, expected_result)

    def test_datetime(self):
        """ Convert to Python datetime """
        gen = time2gen(lst_simple, "simple", "datetime")
        result = []
        for x in gen:
            result.append(x)

        expected_result = [datetime.datetime(2011, 4, 12, 0, 0),
                           datetime.datetime(2012, 8, 3, 0, 0),
                           datetime.datetime(2013, 1, 21, 0, 0)]

#         prints as: ['2011-04-12 00:00:00',
#                     '2012-08-03 00:00:00',
#                     '2013-01-21 00:00:00']

        self.assertEqual(result, expected_result)

    def test_filesfx(self):
        """ Convert to preferred file suffix timestamp """
        gen = time2gen(lst_simple, "simple", "filesfx")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['20110412',
                           '20120803',
                           '20130121']

        self.assertEqual(result, expected_result)


class TestMssql(unittest.TestCase):
    """ Test suite for time2gen 'mssql' generator. """

    def test_simple(self):
        """ Convert to simple shorthand date """
        gen = time2gen(lst_mssql, "mssql", "simple")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011',
                           '08/03/2012',
                           '01/21/2013']

        self.assertEqual(result, expected_result)

    def test_epoch(self):
        """ Convert to Unix epoch integer """
        gen = time2gen(lst_mssql, "mssql", "epoch")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['1302612201',
                           '1344024806',
                           '1358751956']

        self.assertEqual(result, expected_result)

    def test_datetime(self):
        """ Convert to Python datetime """
        gen = time2gen(lst_mssql, "mssql", "datetime")
        result = []
        for x in gen:
            result.append(x)

        expected_result = [datetime.datetime(2011, 4, 12, 8, 43, 21),
                           datetime.datetime(2012, 8, 3, 16, 13, 26),
                           datetime.datetime(2013, 1, 21, 2, 5, 56)]

#         prints as: ['2011-04-12 08:43:21',
#                     '2012-08-03 16:13:26',
#                     '2013-01-21 02:05:56']

        self.assertEqual(result, expected_result)

    def test_filesfx(self):
        """ Convert to preferred file suffix timestamp """
        gen = time2gen(lst_mssql, "mssql", "filesfx")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['20110412',
                           '20120803',
                           '20130121']

        self.assertEqual(result, expected_result)


class TestEpoch(unittest.TestCase):
    """ Test suite for time2gen 'epoch' generator. """

    def test_mssql(self):
        """ Convert to MS SQLServer timestamp """
        gen = time2gen(lst_epoch, "epoch", "mssql")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011 08:43:21 AM',
                           '08/03/2012 04:13:26 PM',
                           '01/21/2013 02:05:56 AM']

        self.assertEqual(result, expected_result)

    def test_simple(self):
        """ Convert to simple shorthand date """
        gen = time2gen(lst_epoch, "epoch", "simple")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011',
                           '08/03/2012',
                           '01/21/2013']

        self.assertEqual(result, expected_result)

    def test_datetime(self):
        """ Convert to Python datetime """
        gen = time2gen(lst_epoch, "epoch", "datetime")
        result = []
        for x in gen:
            result.append(x)

        expected_result = [datetime.datetime(2011, 4, 12, 8, 43, 21),
                           datetime.datetime(2012, 8, 3, 16, 13, 26),
                           datetime.datetime(2013, 1, 21, 2, 5, 56)]

#         prints as: ['2011-04-12 08:43:21',
#                     '2012-08-03 16:13:26',
#                     '2013-01-21 02:05:56']

        self.assertEqual(result, expected_result)

    def test_filesfx(self):
        """ Convert to preferred file suffix timestamp """
        gen = time2gen(lst_epoch, "epoch", "filesfx")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['20110412',
                           '20120803',
                           '20130121']

        self.assertEqual(result, expected_result)


class TestFileSfx(unittest.TestCase):
    """ Test suite for time2gen 'filesfx' generator. """

    def test_mssql(self):
        """ Convert to MS SQLServer timestamp """
        gen = time2gen(lst_filesfx, "filesfx", "mssql")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011 12:00:00 AM',
                           '08/03/2012 12:00:00 AM',
                           '01/21/2013 12:00:00 AM']

        self.assertEqual(result, expected_result)

    def test_simple(self):
        """ Convert to simple shorthand date """
        gen = time2gen(lst_filesfx, "filesfx", "simple")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011',
                           '08/03/2012',
                           '01/21/2013']

        self.assertEqual(result, expected_result)

    def test_datetime(self):
        """ Convert to Python datetime """
        gen = time2gen(lst_filesfx, "filesfx", "datetime")
        result = []
        for x in gen:
            result.append(x)

        expected_result = [datetime.datetime(2011, 4, 12, 0, 0),
                           datetime.datetime(2012, 8, 3, 0, 0),
                           datetime.datetime(2013, 1, 21, 0, 0)]

#         prints as: ['2011-04-12 00:00:00',
#                     '2012-08-03 00:00:00',
#                     '2013-01-21 00:00:00']

        self.assertEqual(result, expected_result)

    def test_epoch(self):
        """ Convert to Unix epoch integer """
        gen = time2gen(lst_filesfx, "filesfx", "epoch")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['1302580800',
                           '1343966400',
                           '1358744400']

        self.assertEqual(result, expected_result)


class TestDateTime(unittest.TestCase):
    """ Test suite for time2gen 'datetime' generator. """

    def test_mssql(self):
        """ Convert to MS SQLServer timestamp """
        gen = time2gen(lst_datetime, "datetime", "mssql")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011 08:43:21 AM',
                           '08/03/2012 04:13:26 PM',
                           '01/21/2013 02:05:56 AM']

        self.assertEqual(result, expected_result)

    def test_simple(self):
        """ Convert to simple shorthand date """
        gen = time2gen(lst_datetime, "datetime", "simple")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['04/12/2011',
                           '08/03/2012',
                           '01/21/2013']

        self.assertEqual(result, expected_result)

    def test_filesfx(self):
        """ Convert to Python datetime """
        gen = time2gen(lst_datetime, "datetime", "filesfx")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['20110412',
                           '20120803',
                           '20130121']

        self.assertEqual(result, expected_result)

    def test_epoch(self):
        """ Convert to Unix epoch integer """
        gen = time2gen(lst_datetime, "datetime", "epoch")
        result = []
        for x in gen:
            result.append(x)

        expected_result = ['1302612201',
                           '1344024806',
                           '1358751956']

        self.assertEqual(result, expected_result)

