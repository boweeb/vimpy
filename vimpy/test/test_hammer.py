# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 28, 2013

@author: Jesse Butcher

test.hammertest:
    Unit test for vimpy.toolbox.hammer

'''

import unittest

import os
import sys
import paramiko as pm
import spur
import tarfile
import re
import csv
import sqlite3
from collections import defaultdict

from vimpy import G
from vimpy.toolbox.wrench import report_to_console, hash_file
from vimpy.toolbox import screwdriver
from vimpy.toolbox import catalog


class TestExplicate(unittest.TestCase):
    """Test suite for 'hammer' module"""

#     def setUp(self):
#         """Set Up"""
#         catalog.init_model()
# 
#     def test_model(self):
#         """Check that mycar was created"""
#         mycar = catalog.Car(make="Dodge",
#                            modl="Dakota",
#                            year="2003",
#                            colr="Dk. Grey")
#         mycar_repr = "<bound method Car.__repr__ of <Car('2003' 'Dodge')>>"
# 
#         repr = str(mycar.__repr__)
# 
#         # Should print Car repr
#         self.assertEqual(repr, mycar_repr)
# 
#     def tearDown(self):
#         pass

#if __name__ == '__main__':
#    unittest.main()
