# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

'''
Created on Mar 28, 2013

@author: Jesse Butcher

vimpy:
    This is the main package container for the VImPy application.

'''

from vimpy.toolbox.jig import GlobalSwatch

# Instantiate my globals
# Import into a module with:
#     "from vimpy import g"
G = GlobalSwatch()
