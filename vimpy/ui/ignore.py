# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Module implementing diaIgnore.
"""

import io
import json
import shutil

from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignature

from vimpy.ui.Ui_ignore import Ui_diaIgnore
from vimpy import G


class diaIgnore(QDialog, Ui_diaIgnore):
    """
    Ignore Dialog
    """
    def __init__(self, parent=None, row=None):
        """ Constructor """
        QDialog.__init__(self, parent)
        self.setupUi(self)

        self.row = row
        row_txt = self.row.data().toString()

        # Load data if file exists, else create empty file and data.
        try:
            with open(G.ignore_file, 'r') as f:
                self.ignores = json.load(f)
        except IOError:
            with open(G.ignore_file, 'w+') as f:
                f.write('{}')
                self.ignores = {}

        self.txtPKID.setText(row_txt)

    @pyqtSignature("")
    def on_buttonBox_accepted(self):
        """ Save 'ignore' record to json file. """
        pkid = self.txtPKID.text().toUtf8().data()
        reason = self.cmbReason.currentText().toUtf8().data()
        self.ignores[pkid] = reason

        # Back up the ignore file.
        shutil.copyfile(G.ignore_file, G.ignore_file + ".bkup")
        try:
            with io.open(G.ignore_file, 'w', encoding='utf-8') as f:
                f.write(unicode(json.dumps(self.ignores,
                                           ensure_ascii = False,
                                           indent = 4,
                                           sort_keys = True)))
        except TypeError, ValueError:
            print "Something went wrong. Restoring backup of 'ignore.json'..."
            shutil.copyfile(G.ignore_file + ".bkup", G.ignore_file)
            raise

#         print self.txtPKID.text()
#         print self.cmbReason.currentText()
