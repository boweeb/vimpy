# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/jesse/workspace/VImPy/vimpy/ui/options.ui'
#
# Created: Wed May  1 11:17:40 2013
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_diaOptions(object):
    def setupUi(self, diaOptions):
        diaOptions.setObjectName(_fromUtf8("diaOptions"))
        diaOptions.resize(600, 229)
        self.verticalLayout = QtGui.QVBoxLayout(diaOptions)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.lohDb = QtGui.QHBoxLayout()
        self.lohDb.setObjectName(_fromUtf8("lohDb"))
        self.grpSrcDb = QtGui.QGroupBox(diaOptions)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.grpSrcDb.sizePolicy().hasHeightForWidth())
        self.grpSrcDb.setSizePolicy(sizePolicy)
        self.grpSrcDb.setObjectName(_fromUtf8("grpSrcDb"))
        self.btnSrcPwUpdate = QtGui.QPushButton(self.grpSrcDb)
        self.btnSrcPwUpdate.setGeometry(QtCore.QRect(220, 85, 61, 23))
        self.btnSrcPwUpdate.setObjectName(_fromUtf8("btnSrcPwUpdate"))
        self.lblSrcSchema = QtGui.QLabel(self.grpSrcDb)
        self.lblSrcSchema.setGeometry(QtCore.QRect(10, 140, 101, 16))
        self.lblSrcSchema.setObjectName(_fromUtf8("lblSrcSchema"))
        self.lblSrcPort = QtGui.QLabel(self.grpSrcDb)
        self.lblSrcPort.setGeometry(QtCore.QRect(10, 120, 101, 16))
        self.lblSrcPort.setObjectName(_fromUtf8("lblSrcPort"))
        self.txtSrcUser = QtGui.QLineEdit(self.grpSrcDb)
        self.txtSrcUser.setGeometry(QtCore.QRect(140, 45, 141, 20))
        self.txtSrcUser.setObjectName(_fromUtf8("txtSrcUser"))
        self.txtSrcPass = QtGui.QLineEdit(self.grpSrcDb)
        self.txtSrcPass.setGeometry(QtCore.QRect(140, 65, 141, 20))
        self.txtSrcPass.setEchoMode(QtGui.QLineEdit.PasswordEchoOnEdit)
        self.txtSrcPass.setObjectName(_fromUtf8("txtSrcPass"))
        self.txtSrcSchema = QtGui.QLineEdit(self.grpSrcDb)
        self.txtSrcSchema.setGeometry(QtCore.QRect(140, 135, 141, 20))
        self.txtSrcSchema.setObjectName(_fromUtf8("txtSrcSchema"))
        self.txtSrcHost = QtGui.QLineEdit(self.grpSrcDb)
        self.txtSrcHost.setGeometry(QtCore.QRect(140, 25, 141, 20))
        self.txtSrcHost.setObjectName(_fromUtf8("txtSrcHost"))
        self.lblSrcHost = QtGui.QLabel(self.grpSrcDb)
        self.lblSrcHost.setGeometry(QtCore.QRect(10, 30, 101, 16))
        self.lblSrcHost.setObjectName(_fromUtf8("lblSrcHost"))
        self.lblSrcPass = QtGui.QLabel(self.grpSrcDb)
        self.lblSrcPass.setGeometry(QtCore.QRect(10, 70, 101, 16))
        self.lblSrcPass.setObjectName(_fromUtf8("lblSrcPass"))
        self.txtSrcPort = QtGui.QLineEdit(self.grpSrcDb)
        self.txtSrcPort.setGeometry(QtCore.QRect(140, 115, 141, 20))
        self.txtSrcPort.setObjectName(_fromUtf8("txtSrcPort"))
        self.lblSrcUser = QtGui.QLabel(self.grpSrcDb)
        self.lblSrcUser.setGeometry(QtCore.QRect(10, 50, 101, 16))
        self.lblSrcUser.setObjectName(_fromUtf8("lblSrcUser"))
        self.lohDb.addWidget(self.grpSrcDb)
        self.grpDstDb = QtGui.QGroupBox(diaOptions)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.grpDstDb.sizePolicy().hasHeightForWidth())
        self.grpDstDb.setSizePolicy(sizePolicy)
        self.grpDstDb.setObjectName(_fromUtf8("grpDstDb"))
        self.txtDstUser = QtGui.QLineEdit(self.grpDstDb)
        self.txtDstUser.setGeometry(QtCore.QRect(140, 40, 141, 20))
        self.txtDstUser.setObjectName(_fromUtf8("txtDstUser"))
        self.lblDstUser = QtGui.QLabel(self.grpDstDb)
        self.lblDstUser.setGeometry(QtCore.QRect(10, 45, 101, 16))
        self.lblDstUser.setObjectName(_fromUtf8("lblDstUser"))
        self.lblDstPort = QtGui.QLabel(self.grpDstDb)
        self.lblDstPort.setGeometry(QtCore.QRect(10, 115, 101, 16))
        self.lblDstPort.setObjectName(_fromUtf8("lblDstPort"))
        self.lblDstPass = QtGui.QLabel(self.grpDstDb)
        self.lblDstPass.setGeometry(QtCore.QRect(10, 65, 101, 16))
        self.lblDstPass.setObjectName(_fromUtf8("lblDstPass"))
        self.lblDstHost = QtGui.QLabel(self.grpDstDb)
        self.lblDstHost.setGeometry(QtCore.QRect(10, 25, 101, 16))
        self.lblDstHost.setObjectName(_fromUtf8("lblDstHost"))
        self.txtDstPass = QtGui.QLineEdit(self.grpDstDb)
        self.txtDstPass.setGeometry(QtCore.QRect(140, 60, 141, 20))
        self.txtDstPass.setEchoMode(QtGui.QLineEdit.PasswordEchoOnEdit)
        self.txtDstPass.setObjectName(_fromUtf8("txtDstPass"))
        self.txtDstHost = QtGui.QLineEdit(self.grpDstDb)
        self.txtDstHost.setGeometry(QtCore.QRect(140, 20, 141, 20))
        self.txtDstHost.setObjectName(_fromUtf8("txtDstHost"))
        self.txtDstPort = QtGui.QLineEdit(self.grpDstDb)
        self.txtDstPort.setGeometry(QtCore.QRect(140, 110, 141, 20))
        self.txtDstPort.setObjectName(_fromUtf8("txtDstPort"))
        self.txtDstDsn = QtGui.QLineEdit(self.grpDstDb)
        self.txtDstDsn.setGeometry(QtCore.QRect(140, 130, 141, 20))
        self.txtDstDsn.setObjectName(_fromUtf8("txtDstDsn"))
        self.lblDstDsn = QtGui.QLabel(self.grpDstDb)
        self.lblDstDsn.setGeometry(QtCore.QRect(10, 135, 101, 16))
        self.lblDstDsn.setObjectName(_fromUtf8("lblDstDsn"))
        self.btnDstPwUpdate = QtGui.QPushButton(self.grpDstDb)
        self.btnDstPwUpdate.setGeometry(QtCore.QRect(220, 80, 61, 23))
        self.btnDstPwUpdate.setObjectName(_fromUtf8("btnDstPwUpdate"))
        self.lohDb.addWidget(self.grpDstDb)
        self.verticalLayout.addLayout(self.lohDb)
        self.lohButtons = QtGui.QHBoxLayout()
        self.lohButtons.setObjectName(_fromUtf8("lohButtons"))
        self.btnBox = QtGui.QDialogButtonBox(diaOptions)
        self.btnBox.setOrientation(QtCore.Qt.Horizontal)
        self.btnBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        self.btnBox.setObjectName(_fromUtf8("btnBox"))
        self.lohButtons.addWidget(self.btnBox)
        self.verticalLayout.addLayout(self.lohButtons)

        self.retranslateUi(diaOptions)
        QtCore.QObject.connect(self.btnBox, QtCore.SIGNAL(_fromUtf8("rejected()")), diaOptions.reject)
        QtCore.QObject.connect(self.btnBox, QtCore.SIGNAL(_fromUtf8("accepted()")), diaOptions.accept)
        QtCore.QMetaObject.connectSlotsByName(diaOptions)

    def retranslateUi(self, diaOptions):
        diaOptions.setWindowTitle(_translate("diaOptions", "Options", None))
        self.grpSrcDb.setTitle(_translate("diaOptions", "Source Database (MySQL)", None))
        self.btnSrcPwUpdate.setText(_translate("diaOptions", "Update", None))
        self.lblSrcSchema.setText(_translate("diaOptions", "Schema:", None))
        self.lblSrcPort.setText(_translate("diaOptions", "Port:", None))
        self.txtSrcPass.setPlaceholderText(_translate("diaOptions", "●●●●●●●●●●", None))
        self.lblSrcHost.setText(_translate("diaOptions", "Host Name or IP:", None))
        self.lblSrcPass.setText(_translate("diaOptions", "Password:", None))
        self.lblSrcUser.setText(_translate("diaOptions", "Username:", None))
        self.grpDstDb.setTitle(_translate("diaOptions", "Destination Database (MS SQL Server)", None))
        self.lblDstUser.setText(_translate("diaOptions", "Username:", None))
        self.lblDstPort.setText(_translate("diaOptions", "Port:", None))
        self.lblDstPass.setText(_translate("diaOptions", "Password:", None))
        self.lblDstHost.setText(_translate("diaOptions", "Host Name or IP:", None))
        self.txtDstPass.setPlaceholderText(_translate("diaOptions", "●●●●●●●●●●", None))
        self.lblDstDsn.setText(_translate("diaOptions", "DSN:", None))
        self.btnDstPwUpdate.setText(_translate("diaOptions", "Update", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    diaOptions = QtGui.QDialog()
    ui = Ui_diaOptions()
    ui.setupUi(diaOptions)
    diaOptions.show()
    sys.exit(app.exec_())

