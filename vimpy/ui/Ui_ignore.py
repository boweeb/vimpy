# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/jesse/workspace/VImPy/vimpy/ui/ignore.ui'
#
# Created: Mon Apr 22 15:08:00 2013
#      by: PyQt4 UI code generator 4.10
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_diaIgnore(object):
    def setupUi(self, diaIgnore):
        diaIgnore.setObjectName(_fromUtf8("diaIgnore"))
        diaIgnore.resize(323, 92)
        self.verticalLayout_3 = QtGui.QVBoxLayout(diaIgnore)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.lohFields = QtGui.QHBoxLayout()
        self.lohFields.setObjectName(_fromUtf8("lohFields"))
        self.lovPKID = QtGui.QVBoxLayout()
        self.lovPKID.setObjectName(_fromUtf8("lovPKID"))
        self.lblPKID = QtGui.QLabel(diaIgnore)
        self.lblPKID.setObjectName(_fromUtf8("lblPKID"))
        self.lovPKID.addWidget(self.lblPKID)
        self.txtPKID = QtGui.QLineEdit(diaIgnore)
        self.txtPKID.setReadOnly(True)
        self.txtPKID.setObjectName(_fromUtf8("txtPKID"))
        self.lovPKID.addWidget(self.txtPKID)
        self.lohFields.addLayout(self.lovPKID)
        self.lovReason = QtGui.QVBoxLayout()
        self.lovReason.setObjectName(_fromUtf8("lovReason"))
        self.lblReason = QtGui.QLabel(diaIgnore)
        self.lblReason.setObjectName(_fromUtf8("lblReason"))
        self.lovReason.addWidget(self.lblReason)
        self.cmbReason = QtGui.QComboBox(diaIgnore)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cmbReason.sizePolicy().hasHeightForWidth())
        self.cmbReason.setSizePolicy(sizePolicy)
        self.cmbReason.setEditable(True)
        self.cmbReason.setObjectName(_fromUtf8("cmbReason"))
        self.cmbReason.addItem(_fromUtf8(""))
        self.cmbReason.addItem(_fromUtf8(""))
        self.lovReason.addWidget(self.cmbReason)
        self.lohFields.addLayout(self.lovReason)
        self.verticalLayout_3.addLayout(self.lohFields)
        self.buttonBox = QtGui.QDialogButtonBox(diaIgnore)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout_3.addWidget(self.buttonBox)

        self.retranslateUi(diaIgnore)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), diaIgnore.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), diaIgnore.reject)
        QtCore.QMetaObject.connectSlotsByName(diaIgnore)

    def retranslateUi(self, diaIgnore):
        diaIgnore.setWindowTitle(_translate("diaIgnore", "Add To Ignore List", None))
        self.lblPKID.setText(_translate("diaIgnore", "PKID", None))
        self.lblReason.setText(_translate("diaIgnore", "Reason", None))
        self.cmbReason.setItemText(0, _translate("diaIgnore", "Duplicate", None))
        self.cmbReason.setItemText(1, _translate("diaIgnore", "Spam", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    diaIgnore = QtGui.QDialog()
    ui = Ui_diaIgnore()
    ui.setupUi(diaIgnore)
    diaIgnore.show()
    sys.exit(app.exec_())

