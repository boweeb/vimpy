# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Module implementing diaOptions.
"""

#TODO: Consider using figs module as configparser wrapper.

from PyQt4.QtGui import QDialog
from PyQt4.QtCore import pyqtSignature

from vimpy.ui.Ui_options import Ui_diaOptions
from vimpy.toolbox.screwdriver import (load_options, save_options, update_pw)


class diaOptions(QDialog, Ui_diaOptions):
    """
    Options Dialog
    """
    def __init__(self, parent=None):
        """ Constructor """
        QDialog.__init__(self, parent)
        self.setupUi(self)

        load_options(self)

    @pyqtSignature("")
    def on_btnBox_accepted(self):
        """
        ButtonBox Accept Slot

        QMessageBox.information(self, "Make PyQt - Failures",
                    "Try manually setting the paths to the tools "
                    "using <b>More-&gt;Options</b>")
        """

        save_options(self)

    @pyqtSignature("")
    def on_btnSrcPwUpdate_clicked(self):
        """ Update source database password. """
        update_pw(self.txtSrcPass, "db_src")

    @pyqtSignature("")
    def on_btnDstPwUpdate_clicked(self):
        """ Update destination database password. """
        update_pw(self.txtDstPass, "db_dst")
    
    @pyqtSignature("QString")
    def on_txtSrcPass_textEdited(self, p0):
        """On text edit, enable the update button."""
        self.btnSrcPwUpdate.setEnabled(True)
    
    @pyqtSignature("QString")
    def on_txtDstPass_textEdited(self, p0):
        """ On text edit, enable the update button. """
        self.btnDstPwUpdate.setEnabled(True)
