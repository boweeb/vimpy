# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
Module implementing Home.
"""

from sqlalchemy import exc

from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QMessageBox
from PyQt4.QtCore import pyqtSignature, QCoreApplication

from vimpy.ui.Ui_home import Ui_MainWindowz
from vimpy.toolbox import hammer
from vimpy.toolbox.wrench import report_to_console as rpt
from vimpy.toolbox.screwdriver import (gen_errorbox,
                                       det_errorbox)


class Home(QMainWindow, Ui_MainWindowz):
    """
    Home
    """
    def __init__(self, parent=None):
        """Constructor"""
        QMainWindow.__init__(self, parent)
        self.setupUi(self)
        # Delay catalog import until form is painted.
        hammer.init_catalog()

        self.tblSrcHeader = ['PKID', 'Application Date',
                             'First Name', 'Last Name', 'Position']        
        self.tblSrcData = None

        self.tblDstHeader = ['PKID', 'Application Date', 'HRRef',
                             'First Name', 'Last Name', 'Position']
        
        rpt(self, "INIT", "Main form painted.")
        rpt(self, "Complete!", "---------------------------------")

    # ~ BUTTONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    @pyqtSignature("")
    def on_btnQueryVP_clicked(self):
        """ Query the local Viewpoint server for data. """
        try:
            self.tblDstData = hammer.init_dst_table(self,
                                                    self.tblDstTable,
                                                    self.tblDstHeader,
                                                    "HRRM")
        except exc.SQLAlchemyError, e:
            msg = ("A database error has occurred. " +
                   "Please check your settings.<br><br>" +
                   "<b>SQLAlchemy Error:</b><br><tt>{}<tt>").format(e)
            gen_errorbox("Database Error", msg)
            rpt(self, "ERROR", msg)
            return False

        try:
            if not self.tblDstData:
                return False
            else:
                self.lcdTotal.display(len(self.tblDstData[1]))
        except AttributeError as e:
            msg = ("The object 'tblDstData' was not created. This " +
                   "indicates the Qt code did not run correctly. Please " +
                   "check the 'home' and 'hammer' modules.")
            det = "Python Error:\n\n\"{}\"\n".format(e)
            det_errorbox("Error", msg, det)
            rpt(self, "ERROR", msg + '\n' + det)
            print("** Render Viewpoint table, failed **\n")
            return False

    @pyqtSignature("")
    def on_btnQuery_clicked(self):
        """ Query the web server's mysqld for data. """
        # If new rows were added, update counter and enable buttons.
        
        try:
            self.tblSrcData = hammer.init_src_table(self, 
                                                    self.tblSrcTable,
                                                    self.tblSrcHeader,
                                                    "Website",
                                                    self.tblDstData)
        except AttributeError, e:
            msg = ("The Source table tried to init before Destination " +
                   "table. Viewpoint must be queried first.")
            det = "Python Error:\n\n\"{}\"\n".format(e)
            det_errorbox("Error", msg, det)
            rpt(self, "ERROR", msg + '\n' + det)
            return False
        except exc.SQLAlchemyError, e:
            msg = ("A database error has occurred. " +
                   "Please check your settings.<br><br>" +
                   "<b>SQLAlchemy Error:</b><br><tt>{}<tt>").format(e)
            gen_errorbox("Database Error", msg)
            rpt(self, "ERROR", msg)
            return False
        QCoreApplication.processEvents()
            
        new_rows = len(self.tblSrcData)
        if new_rows > 0:
            self.btnImport.setEnabled(True)
            self.btnIgnore.setEnabled(True)
            rpt(self, "\tINFO", "{} new records.".format(new_rows))
        elif new_rows == 0:
            rpt(self, "\tINFO", "No new records.")

        self.lcdNew.setEnabled(True)
        self.lblNew.setEnabled(True)
        self.lcdNew.display(new_rows)
        
        rpt(self, "Complete!", "---------------------------------")

    @pyqtSignature("")
    def on_btnImport_clicked(self):
        """ Import the selected items into the destination db. """

        # Collect selected rows' primary keys.
        selection = self.tblSrcTable.selectionModel()
        rows = selection.selectedRows()

        result = hammer.add_src_data(self, rows)
        if result == 0:
            rpt(self, "Complete!", "---------------------------------")
            # Refresh tables.
            rpt(self, "Refreshing Tables", "****************************")
            self.tblDstData = hammer.init_dst_table(self, 
                                                    self.tblDstTable,
                                                    self.tblDstHeader,
                                                    "HRRM")
            self.lcdTotal.display(len(self.tblDstData[1]))
            self.on_btnQuery_clicked()

    @pyqtSignature("")
    def on_btnIgnore_clicked(self):
        """ Add selected item to the ignore list. """
        
        # Collect selected rows' primary keys.
        selection = self.tblSrcTable.selectionModel()
        rows = selection.selectedRows()
        
        from vimpy.ui.ignore import diaIgnore
        
        for row in rows:
            dlg = diaIgnore(self, row)
            dlg.exec_()
        
        self.on_btnQuery_clicked()


    # ~ MENU ACTIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

    @pyqtSignature("")
    def on_actionOptions_triggered(self):
        """Open the Options dialog."""
        from vimpy.ui.options import diaOptions
        dlg = diaOptions(self)
        dlg.exec_()

    @pyqtSignature("")
    def on_actionAbout_triggered(self):
        """About Dialog"""

        QMessageBox.about(self,
                          self.trUtf8("About VImPy"),
                          self.trUtf8("""
                       <b>VImPy</b><br>
                    Viewpoint Importer (Python)<br>
                    <br>
                    <b>Name</b>:<br>
                    An application that *imports* data into the *Viewpoint*
                    server that is written in the *Python* high-level
                    programming language.<br>
                    <br>
                    <b>Synopsis</b>:<br>
                    This application checks the web server MySQL database server
                    which contains applicant information. Then it downloads the
                    data and imports it directly into Viewpoint's SQL Server
                    backend.<br>
                    <br>
                    <b>Contact</b>:<br>
                    Jesse Butcher<br>
                    (614) 595-7159<br>
                    j.butcher@setterlin.com<br>
                    jesse.m.butcher@gmail.com"""))
