===========
AUTHORS.rst
===========

CREATOR
-------
VImPy was created by Jesse Butcher <boweeb@gmail.com>

CONTRIBUTORS
------------
At the time of this writing, there are no other major contributors specific
to VImPy. 

ACKNOWLEDGMENTS
---------------
VImPy uses the following modules/projects and would like to thank their
authors, in no particular order:
- SQLAlchemy
- Spur
- Paramiko
- PyQt
- PyDev
- Python
- Eric IDE
- Ninja-IDE
- re
- csv
- sqlite3
- tarfile
