@echo off

CD "C:\Users\administrator\Downloads\pyinstaller-2.0"
RMDIR /S /Q vimpy\build
RMDIR /S /Q vimpy\dist

pyinstaller.py vimpy/vimpy.spec
PAUSE
