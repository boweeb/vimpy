# -*- mode: python -*-
a = Analysis(['C:\\Users\\administrator\\Desktop\\jbutcher\\vimpy\\main.py'],
             pathex=['C:\\Users\\administrator\\Desktop\\jbutcher\\vimpy',
			         'C:\\Users\\administrator\\Downloads\\pyinstaller-2.0',
					 'C:\\Python27\\Lib\\site-packages'],
             hiddenimports=['pyodbc'],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\vimpy', 'vimpy.exe'),
          debug=False,
          strip=None,
          upx=False,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=False,
               name=os.path.join('dist', 'vimpy'))
