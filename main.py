#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

"""
===========================
VImPy
===========================
---------------------------
Viewpoint Importer (Python)
---------------------------

Copyright (c) 2013, Jesse Butcher
See LICENSE.txt

Last compile:
- QT     4.8.4
- Python 2.7.3
- PyQt   4.9.6

::

    888     8888888888             8888888b.
    888     888  888               888   Y88b
    888     888  888               888    888
    Y88b   d88P  888  88888b.d88b. 888   d88P888  888
     Y88b d88P   888  888 "888 "88b8888888P" 888  888
      Y88o88P    888  888  888  888888       888  888
       Y888P     888  888  888  888888       Y88b 888
        Y8P    8888888888  888  888888        "Y88888
                                                  888
                                             Y8b d88P
                                              "Y88P"

::

    This file is part of VImPy.

    VImPy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VImPy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VImPy.  If not, see <http://www.gnu.org/licenses/>.

"""

import sys

from PyQt4 import QtGui


if __name__ == "__main__":
    print "=-=-= Starting VImPy =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
    app = QtGui.QApplication(sys.argv)
    
    # Moved this import down so the app will be created first.
    from vimpy.ui.home import Home
    
    ui = Home()
    ui.show()
    
    sys.exit(app.exec_())
