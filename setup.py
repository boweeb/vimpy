from distutils.core import setup

setup(
    name='VImPy',
    version='0.5.1',
    author='Jesse Butcher',
    author_email='boweeb@gmail.com',
    packages=['vimpy'],
    scripts=['bin/main.py'],
    url='https://bitbucket.org/boweeb/vimpy',
    license='LICENSE.txt',
    description='Viewpoint Importer for Python',
    long_description=open('README.txt').read(),
    install_requires=[
        'PyQt4', 'sqlalchemy',
    ],
)

